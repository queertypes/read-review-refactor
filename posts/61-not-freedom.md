---
title: Not Freedom
date: April 2, 2020
---

A short poem that came to me while thinking about the sort of
"freedom" we are sold in the u.s..

---

Not freedom,  
as in markets and false democracies,  
where our lives are traded, measured in dollars and pennies,  
and reforms come too few, and too late.  

Not freedom,  
as in predatory landlords with their rows of "properties",  
while many find themselves without shelter or house,  
and many more lack any place to call home.  

Not freedom,  
as in "comforts" sold to us upon the bloody platter of imperialist theft,  
snake oil in exchange for the lives of our siblings and comrades,  
here and abroad.  

Not freedom,  
as in life barely lived, paycheck to paycheck,  
alienated from our own labor,  
and always one misstep away from misery.  

But liberation!  

Liberation!  
as in the right to a dignified, fulfilling, loving life for all.  

Liberation!  
as in organizing in revolutionary solidarity against all oppressors.  

Liberation!  
as in Get Your Boots Off Our Gods Damned Necks!  

Liberation!  
as in never again will they own our lives.  

Towards liberation, always. So that we may be truly free.
