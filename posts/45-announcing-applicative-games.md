-----
title: Announcing -  Applicative Games
date: February 8, 2015
tags: haskell, game development
-----

Hi all!

I'm happy to announce
[Applicative Game Development](https://applicative-games.queertypes.com/),
a blog dedicated to the exploration of purely functional game
development.

I've been doing some exploration in that area lately. I want to make
it easier for others to navigate that space, as there's a paucity of
resources.

Why a new blog? I wanted to create a dedicated space. Here, you'll see
an overlap between my academic, professional, and personal pursuits. I
wanted a space that was all about game development.

If you're curious, check out the new blog
[here](https://applicative-games.queertypes.com/).
