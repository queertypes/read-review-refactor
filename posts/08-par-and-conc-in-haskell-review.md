-----
date: December 12, 2013
title: Parallel and Concurrent Programming in Haskell by Simon Marlow; O'Reilly Media;
tags: haskell, review
-----

I want to start writing this, but it's been forever since I actually
read this book. I'm concerned I won't be able to do it justice.

So here's the deal: I've read this book once many months ago (June?),
and I've been re-reading it since. Some of the details are blurry, but
the gist of the book is still with me.

Without further ado, here's the review:

## Review

Disclaimer: I'm reviewing this book under the O'Reilly Blogger Review
program. (though I ended up purchasing a hard copy afterwards any
way.)

This is The Book that sold me on Haskell for concurrent and parallel
programming. Sure, I've read several articles on the benefits of
functional languages for programming in the multi-core world, but that
didn't really sink in until I saw how elegant it could be in a
functional language.

In brief, the main benefits I got from reading this this book were:

* Surveyed parallel programming (in Haskell)
* Surveyed concurrent programming (in Haskell)
* Saw the elegance of the approaches for myself
* Learned about laziness gotchas in parallel contexts
* Learned a bit about what's next and left to improve
* Learned what modules to turn to and watch when in need

I hope I never have to look at OpenCL or CUDA C++ again for parallel
programming. The way Repa/Accelerate handles this is beautiful.

The chapters on concurrent programming showed me how much having
concurrency primitives built into a language change async
programming. Having ```forkIO``` to run subsequent computations and a
scheduler in the run-time make it very convenient.

In sum, I highly recommend this book. 10/10, one of my top 10 books of
2013.
