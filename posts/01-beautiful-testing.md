-----
title: Beautiful Testing by Adam Goucher, Tim Riley; O'Reilly Media
date: February 9, 2013
tags: review, books, testing
-----

![](http://akamaicovers.oreilly.com/images/9780596159825/cat.gif)
Disclaimer: I've read portions of the book, not all of it. I'll
specify these sections as I review. I'm reviewing the eBook
version. (The PDF is great!). I received this book as a gift for
participating in an O'Reilly Velocity survey.

To begin with, I found that this book is very friendly to jump into
any chapter at any time - there are no ordering dependencies. This is
very nice, since at times I'll be more interested in the business
aspect and at others I want to do a technical deep dive. The quality
of the writing has also been mostly solid so far. The author's
passions shine through in most cases, and there has only been one
section where it felt like the writing was too dry.

There are three parts to this book: Beautiful Testers, Beautiful
Process, and Beautiful Tools. I found this division into tracks to be
very effective, and further facilitates jumping in depending on one's
mood and needs.

In preparation to review this book, I read the following chapters:

* (1) **Was It Good For You?**
* (3) **Building Open Source QA Communities**
* (4) **Collaboration is the Cornerstone of Beautiful Performance Testing**
* (14) **Test-Driven Development: Driving New Standards of Beauty**
* (15) **Beautiful Testing as the Cornerstone of Business Success**
* (17) **Beautiful Testing is Efficient Testing**

**Was It Good For You?** is an opinionated and energizing introduction
to this book. In this chapter, Linda Wilkinson attempts to define what
a tester is and what value a tester brings to the table. It felt like
a keynote at a major conference, and left me feeling a little inspired
after I read through it. Even though there are no ordering
dependencies, I highly recommend starting at this chapter. It adds a
lot to the book.

**Building Open Source QA Communities** is an interesting tale of what
it takes to get volunteers involved and engaged, and what it takes to
lose them. It was a memorable read.

**Collaboration is the Cornerstone of Beautiful Performance Testing** is
all about working with others. It tells a story about how difficult
things can be when miscommunication arises, and how a little effort in
collaboration goes a long way towards really understanding the
requirements. The back-and-forth dynamics detailed in the chapter make
for an entertaining read.

The three chapters in the **Beautiful Processes** part of the book dove
in. Chapter 17 was particularly pleasant to read. It was concise and
humorous, and presented a neat mnemonic for determining testing
priority (SLIME).

I've not read any part of the Beautiful Tools section of the book, but
the chapter on **"Testing Network Services in Multimachine Scenarios"**
looks particularly interesting.

Another note about the book overall: there are many great diagrams
throughout, in full color. These make for convenient print-outs that
summarize the knowledge in a chapter!

What could be better? I feel like the balance between parts could have
been a bit better. I would have appreciated a little more on the side
of Beautiful Tools. The landscape of testing tools only continues to
grow. In fact, if another edition of this book is ever released, that
would be at the top of my wishlist for new content!

Check it out: [Beautiful Testing](http://shop.oreilly.com/product/9780596159825.do)
