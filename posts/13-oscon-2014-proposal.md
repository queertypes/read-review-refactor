-----
date: January 30, 2014
title: My OSCON 2014 Proposal -  The Case for Haskell
tags: haskell, oscon
-----

I just finished submitting my proposal for OSCON 2014. After two days
of brainstorming, I feel pretty good about what I've come up
with. This talk:

1. Is about something I'm passionate about
2. Is a talk I would give even if I wasn't accepted
3. Requires me to learn even more thoroughly what I'm proposing to
   speak on It's an opportunity to learn!

For those of you that would like to submit a proposal in the future, I
have two thoughts to share with you.

First - **DO IT!** If you have something you'd love to share, get it out
there. You are [Allowed to Apply](http://allowedtoapply.tumblr.com/).

Secondly, if you need a simple video recording solution (I was on
Linux, I used my laptop web cam, and I lacked a proper camera), I'm
recommending the [YouTube Recording](https://www.youtube.com/my_webcam) interface. Given the capabilities
of HTML5, I wasn't entirely surprised that such a thing
existed. However, I was pleased with the outcome. It certainly worked
for me, and worked better than either Cheese of Guvcview.


More on the process:

I must've re-re-recorded myself at least 10 times in trying to express
my abstract. It took practice. It was scary the first time around. I
stumbled on words. I ran out of breath. I forgot where I was. It got
easier as I got into the cycle of editing my script, tweaking,
optimizing, and simplifying. It was very similar to developing,
testing, and refactoring. There is Zen in all of this.

So that was it. Lots and lots of practice. Now, the long wait.

Oh, and by the way - sharing is caring: here's the link to watch my
video proposal: [The Case for Haskell](https://www.youtube.com/watch?v=L3mJbHGrLdw)

Let me know what you think. As suggested by (2) above, I intend to
give this talk whether or not I'm accepted to speak at OSCON, so let
me know if there's something that you'd love to hear about!
