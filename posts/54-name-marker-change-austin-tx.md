----------
date: October 16, 2015
title: Notes on -  Name and Marker Change in Austin, TX
tags: transgender, government, notes
----------

I went to a free workshop today focused on the process needed to
change one's name and gender marker in Austin, TX. It turned out that
it was a bit more than that! The very brief of it:

* They gave a short overview of the program
* They paired me with legal students
* They began the process of filling out the relevant petitions
* They made copies of necessary documents
* They gave me a take-home list of docs I still needed
* They'll keep in touch during the process and help guide it to completion

I asked about support for non-binary gender identities. As I expected,
there is no such support in the United States of America. You pretty
much choose M or F. It's amazing how much pressure this puts on
individuals to "sort" into the binary. More on that some other time.

Here's the sorts of documents needed for completing the process:

* Social security card
* Birth certificate
* Letter from therapist
* Letter from physician
* State issued ID
* Fingerprint card

There seems to be additional documentation needed if you've changed
your name before or if you've got a criminal record.

They reassured listeners that the judges in Austin, TX would not have
an adversarial hearing. That's to say, they won't ask you why you're
doing this, what your gender is, etc. They'll review your petition
beforehand, and then you just need to be present in order to answer
questions about missing documentation or documentation that needs
clarification.

Another detail: as a resident of TX, you can choose any county in TX
to undertake the procedure. Austin, TX rates favorably, based on
information I overheard at the workshop. Also, if you were born in TX,
but then move to another state, you can still complete this process in
TX, even as a non-resident.

The expected fees for the full process are around $300-$400. These
fees can be waived if you show that you'd be unable to pay them.

The most tedious process really seems to be about the aftermath -
everything **ELSE** you need to change after getting legal recognition
from the local government. For me, that amounts to at least:

* Marriage license
* Lease
* Vehicle registration
* Social security
* Driver's license
* Business ownership
* Bank accounts
* Credit reporting agencies

That's the gist of things. [The Q](http://www.theqaustin.org/) hosted
the workshop, and they seem pretty great.

So with that, my name will be Allele Dev in probably about three to
four months, and my gender marker will be F.
