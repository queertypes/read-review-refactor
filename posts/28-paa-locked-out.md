-----
title: Programming as Art -  Locked Out
date: August 12, 2014
tags: programming as art, haskell, security
-----

This is an experiment. The idea struck me this morning after playing
with [haskeline](https://hackage.haskell.org/package/haskeline) last
night.

Below's the first result: a simple program that demonstrates the
frustration of trying to get into your account using a password - like
trying to SSH into one of your own servers, trying to log on to a
financial services site, or even a government/city page.

Best wishes. May your (rightful) log in attempts always succeed.

```haskell
{-
Run with:
$ runHaskell LockedOut.Hs

Quit with: Ctrl-C

Try as you may, you can't remember your account password.
You try a combination:

"Access denied. Try again."

Perhaps it was that one...?

"Access denied. Try again."

Or maybe this was one of those passwords that had wonky requirements...?

"Access denied. Try again."
"Access denied. Try again."
"Access denied. Try again."

Authentication - not yet a solved problem.
-}
module LockedOut where
import System.Console.Haskeline

loop :: IO ()
loop = do
  _ <- runInputT defaultSettings $ getPassword (Just '*') "> "
  putStrLn "Access denied. Try again."
  loop

main :: IO ()
main = loop
```
