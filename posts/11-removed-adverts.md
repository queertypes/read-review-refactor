-----
date: January 13, 2014
title: Removed the Advertising
tags: meta,
-----

Dear readers,

I've removed the O'Reilly widget from the right hand side of the
page. My reason for doing so is that I don't want to bombard you with
advertising every time you visit my blog. I want you to feel safe to
come here to gather whatever information I make available.

The influence of advertising is subtle. It works even better when it
takes a background role in your subconscious. I deliberately choose
not to watch cable television in large part because of this effect. I
don't want to be craving a taco salad or a chicken sandwich late in
the evening and not know where that craving came from! I want it to be
my choice (as much as possible) if I want such things.

So read on, and soak in what you like (or don't like). I'll keep
writing, and this place will be ad-free. If for whatever reason, this
blogging platform begins to introduce advertisements, then I'll host
this blog myself.

Of course, I'll continue to review books, both by the grace of blogger
review programs and by my own love of books. Ah, on that note - expect
a review of Real World Haskell soon. I just finished the book up for
the second time, and now feel ready to write about it.

Enjoy!
