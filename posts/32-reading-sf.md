-----
title: Reading - Software Foundations
date: September 04, 2014
tags: sf, types, books
-----

This is just a tiny announcement. I'm going to start reading
[Software Foundations](http://www.cis.upenn.edu/~bcpierce/sf/current/index.html). I've
heard great things about it, and I want to pick up a theorem prover in
the process.

If you'd like to join in and read with me, there's an online reading
group hosted at the [HaskellNow](http://www.haskellnow.org/)
[wiki](http://www.haskellnow.org/wiki/Software_Foundations).

Here's some sample Coq to start the adventure.

```haskell
Inductive day : Type :=
  | monday : day
  | tuesday : day
  | wednesday : day
  | thursday : day
  | friday : day
  | saturday : day
  | sunday : day.

Definition next_weekday (d:day) : day :=
    match d with
      | monday => tuesday
      | tuesday => wednesday
      | wednesday => thursday
      | thursday => friday
      | friday => monday
      | saturday => monday
      | sunday => monday
    end.


Inductive bool : Type :=
  | true : bool
  | false : bool.

Definition negb (b:bool) : bool :=
  match b with
    | true => false
    | false => true
  end.

Definition andb (b1:bool) (b2:bool) : bool :=
  match b1 with
    | true => b2
    | false => false
  end.

Definition orb (b1:bool) (b2:bool) : bool :=
  match b1 with
    | false => b2
    | true => true
  end.

Example test_orb1: (orb true false) = true.
Proof. reflexivity. Qed.
Example test_orb2: (orb false false) = false.
Proof. reflexivity. Qed.
Example test_orb3: (orb false true) = true.
Proof. reflexivity. Qed.
Example test_orb4: (orb true true) = true.
Proof. reflexivity. Qed.
```
