-----
title: Updates and Upcoming Reviews
date: February 15, 2013
tags: review, books
-----

It's been a busy week, but there's still time to read available. I've
just finished reading
[Hackers and Painters](http://shop.oreilly.com/product/9780596006624.do)
by Paul Graham. I'll have a review of that one up by the end of the
weekend. Up next, I'm looking at (in no certain order):

* [Working Effectively with Legacy Code](http://www.amazon.com/Working-Effectively-Legacy-Michael-Feathers/dp/0131177052), Michael C. Feathers
* [Refactoring: Improving the Design of Existing Code](http://www.amazon.com/Refactoring-Improving-Design-Existing-Code/dp/0201485672), Martin Fowler
* [Beautiful Architecture](http://shop.oreilly.com/product/9780596517984.do), Diomidis Spinellis, Georgios Gousios
* [Introducing Erlang](http://shop.oreilly.com/product/0636920025818.do), Simon St. Laurent

Happy reading!
