-----
date: May 5, 2014
title: Announce -  A New Job (Haskell), Moving (Austin, TX)
tags: me, haskell
-----

Hello all,

There's been a bit going on in the professional landscape over the
past few months. I decided that I wanted to find a way to bridge my
academic interests and my professional interests. This blog post is
about what I did to get there and what's next for me.

tl;dr - I'll be working as a Haskell programmer for Rackspace in
Austin, TX. I begin as a remote worker at the end of this month and
move to Austin, TX at the start of September! Read on for the details.

If you've been reading this blog, you may have noticed that I have an
[interest](/posts/ten-ways-haskell-education.html) in typed
[functional](/posts/oscon-2014-aspire.html)
[programming](/posts/oscon-2014-proposal.html), by way of
[Haskell](/tags/haskell.html). I'm increasingly interested, and I
think it's a really exciting direction for the future. Speaking
personally, it's revived my interest in mathematics, and really gives
me hope for bridging the gap between research and application in the
field of computer science.

For the past year, I've been working as a Python developer for
Rackspace. I've helped out a bit with the
[Openstack Marconi](https://wiki.openstack.org/wiki/Marconi) project,
a queuing-as-a-service over HTTP kind of dig. The community for this
project is wonderful, and I've enjoyed many weeks of poptart jokes,
discussion on all matter of things, and making wonderful friends in
the process. I've learned a lot, too. This project operated at the
intersection of Python lore, distributed systems, and mid-scale
software engineering. I learned how to consider design for sustainable
testing, what it means to make a system easy to deploy and provision
robustly, and the kinds of considerations that go into compatibility
across versions - all sorts of things that I'll carry with me wherever
I go.

However, I realized it was time to move on when I saw my primary
interests diverging. I wanted to see what it means to build systems
using typed, functional programming languages. It's been great to
advocate for their use for the past few months, and my talk to OSCON
was even accepted, but I want to taste the "Real World". I told myself
a number of times: "I'm willing to bet my career on Haskell". And so I
did, and it came to pass that I found another position where I would
have the opportunity to use Haskell to solve problems.

What did it take to find this position? A lot of networking, really. I
started by asking around at Rackspace whether functional programming
was being used. The answer was in the positive, and during an internal
conference, I met Lispers, Scala-ers, Clojurists, Erlangers, and
Haskellers. After much geeking out, I inquired whether any of these
teams were looking for a helping hand. The answer was in the positive,
and the rest is a history I'd be happy to share over lunch.

A quick aside on OSCON - if not for the
[Allowed to Apply](http://allowedtoapply.tumblr.com/) Tumblr, I would
not have submitted a proposal. This collection of inspirational
mini-posts encouraged me to share how strongly I felt about Haskell
with the world, before I could even properly reason about the magic of
Monads. I would've readily convinced myself that because I lacked
experience, that my perspective on the matter had no value. I was
afraid to speak at such any sufficiently public venue. That's
[Impostor Syndrome](http://en.wikipedia.org/wiki/Impostor_syndrome) in
action, which [Julie Pagano](http://juliepagano.com/) addresses
wonderfully in this
[recent talk](http://pyvideo.org/video/2659/its-dangerous-to-go-alone-battling-the-invisibl). I'm
still preparing that talk for July 22nd, and I hope to bring a
balanced, sobering view then, that goes beyond Haskell and speaks to
the enabling powers of typed functional programming languages.

So here I am, in the middle of a transition. I start officially come
the end of May in this new Haskell-y position. I've wrapped up most of
what I started for Rackspace with regards to the Marconi
project. However, there's still one very important set of loose
threads - the
[Google Summer of Code](https://developers.google.com/open-source/soc/?csw=1)
and the [Gnome OPW](https://gnome.org/opw/) programs. Let me address
these directly.

I'll continue to contribute to the Marconi project as a mentor 'til
the end of the end of these programs. I want to pass on what I've
learned to the next generation of amazing programmers. I participated
in the Google Summer of Code in the Summer of 2011 as a student for
the Boost C++ community. A vaguely functional
[Bloom filter](https://github.com/queertypes/boost-bloom-filters) emerged
from that effort, but that's not the important part. That was my first
experience with open-source development. I want to pass on more than
just technical knowledge during GSoC/OPW - I want to pass on a love
for sharing and learning together. We don't have to go it alone - and
so I'll do my best to enable those that I'll be working with. Let's
make the best of it!

To bring this to a close, there are new ventures in my future. I hope
to learn a lot in the process. No doubt, readers - you'll continue to
hear from me on this blog at least as often as now. All that'll have
changed is that my professional interests will be realigned with my
personal interests. Only good can come of that.
