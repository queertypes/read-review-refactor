-----
title: 21st Century C by Ben Klemens; O'Reilly Media
date: July 22, 2013
tags: books, review, c
-----

Disclaimer: I've received this book as a part of the O'Reilly Blogger
Review Program.

In **21st Century C**, Ben Klemens sets out to show you how much C has
changed over the past two decades. Promising to show you how to make
the most of modern C, he takes on a tour of the language, including
features added by recent standards, and also indicates what features
we should stop using.

Verdict: ```[||||||----]``` (6 out of 10)

tl;dr - I wouldn't recommend this book in general. If you're looking
for idiomatic C, and you're experienced enough with C to know what
works and what doesn't, and are able to make judgment on a
case-by-case basis with regards to the author's advice, you might be
able to find a few interesting gems in here.

For the reader interested in a more detailed review:

I found Klemens' writing to be entertaining. The references to punk
rock became a little groan-inducing after some time, but groans are
better than snores - I wasn't put to sleep by the writing. His
character definitely shows through his writing. I appreciate this, as
all too many text books suffer from the formalism-first
problem. Readers are humans, and Klemens addresses this well.

The tour of C history was also very interesting. The coverage of C
from inception up to the latest standard (C11) and how things have
changed since then was especially valuable to me. I even appreciated
the mild coverage that was given regarding C on Windows, where C
really seems to be an afterthought.

One more good point before I jump into the criticisms - the chapter
covering pointer semantics was solid. Klemens explains how to use
them, several of the associated gotchas, and what to look out
for. Succinct, clear, and full of examples.

Things are not so bright every where in the book. One particular
section that concerned me, was in Chapter 1: The Unified
Header. Klemens contends that every project should use an
```allheaders.h``` because "Your compiler doesn't think 2,400 lines is
a big deal any more, and this compiles in under a second". While I
agree that compilation speed won't be a problem, the unified header
approach has a few problems that are more troublesome for long-term
maintenance. First and foremost, since C has very limited support for
name-spacing, it'll be very difficult to new comers to your C library
to determine where each imported function and structure comes
from. When time to refactor comes along, it'll be more likely that the
refactoring process will be difficult if the reader follows this
unified header approach than if they made more clear separation of
their modules by purpose, relationship, and abstraction level.

I leave you with this final note - one of the strengths of this book
is that it is very opinionated. Unfortunately, this is also this
book's greatest weakness. Take every claim made within with a grain of
salt and you should emerge wiser.
