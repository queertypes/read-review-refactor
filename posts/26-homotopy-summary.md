-----
title: Homotopy, Sets, Logic, and Types
date: July 17, 2014
tags: type theory, homotopy
-----

I've started studying homotopy type theory in earnest. What started
six months ago as an expedition into Haskell and type systems now
takes me deeper and deeper into the underlying mathematics. The
reasons aren't strictly because I believe this to be of benefit to my
programming. It is more because I've grown fond of structures,
algebras, categories, and more as I delve in typed functional
programming. Math strikes me as **really** interesting, and I didn't
realize this **until** I started exploring functional programming and
inductive reasoning.

For the time being, I've duplicated a nifty table from the
[Homotopy Type Theory Book](http://homotopytypetheory.org/book/)
below. It summarizes the equivalences between types, logic, sets, ands
homotopy, much in the spirit of the
[Curry-Howard-Lambek](http://en.wikipedia.org/wiki/Curry%E2%80%93Howard_correspondence#Curry.E2.80.93Howard.E2.80.93Lambek_correspondence)
isomorphism or its intuitionistic logic counterpart the
[Brouwer-Heyting-Kolomogriv correspondence](http://en.wikipedia.org/wiki/Brouwer%E2%80%93Heyting%E2%80%93Kolmogorov_interpretation).

Enjoy!

+-----------------------+------------------------+--------------------------------+-----------------------+
| Types                 | Logic                  | Sets                           | Homotopy              |
+=======================+========================+================================+=======================+
| $A$                   | proposition            | set                            | space                 |
+-----------------------+------------------------+--------------------------------+-----------------------+
| $a : A$               | proof                  | element                        | point                 |
+-----------------------+------------------------+--------------------------------+-----------------------+
| $B(x)$                | predicate              | family of sets                 | fibration             |
+-----------------------+------------------------+--------------------------------+-----------------------+
| $b(x) : B(x)$         | conditional proof      | family of elements             | section               |
+-----------------------+------------------------+--------------------------------+-----------------------+
| 0,1                   | $\bot$ , $\top$        | $\emptyset$, {$\emptyset$}     | $\emptyset$, $\ast$   |
+-----------------------+------------------------+--------------------------------+-----------------------+
| $A + B$               | $A \vee B$             | disjoint union                 | coproduct             |
+-----------------------+------------------------+--------------------------------+-----------------------+
| $A \times B$          | $A \wedge B$           | set of pairs                   | product space         |
+-----------------------+------------------------+--------------------------------+-----------------------+
| $A \rightarrow B$     | $A \Rightarrow B$      | set of functions               | function space        |
+-----------------------+------------------------+--------------------------------+-----------------------+
| $\Sigma_{(x:A)}B(x)$  | $\exists_{x:A}B(x)$    | disjoint sum                   | total space           |
+-----------------------+------------------------+--------------------------------+-----------------------+
| $\Pi_{(x:A)}B(x)$     | $\forall_{x:A}B(x)$    | product                        | space of sections     |
+-----------------------+------------------------+--------------------------------+-----------------------+
| $Id_{A}$              | $equality =$           | $\{(x,x) | x \in A\}$          | path space $A^{I}$    |
+-----------------------+------------------------+--------------------------------+-----------------------+
