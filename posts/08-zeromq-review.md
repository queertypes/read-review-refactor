-----
date: August 21, 2013
title: ZeroMQ - Messaging for Many Application by Peter Hintjens; O'Reilly Media
tags: books, review, zeromq
-----

## Building Distributed Systems and Communities

I was pleasantly surprised by this book. I came in expecting to learn
about the magic of ZeroMQ, and I came out knowing not only more about
ZeroMQ itself, but also distributed systems and community building.

The first chapter primes the rest of the text, preparing the reader to
not only learn more about the world of distributed computing, but also
begins instilling a certain excitement about how things could be so
much easier, and how ZeroMQ enables this. This is surprising, as very
few technical texts manage to become page-turners so quickly.

The next four chapters are a deep dive into ZeroMQ (and distributed
systems) best practices. Heartbeats, publish/subscribe, request/reply,
round-robin task distribution, and then composing all of these
patterns and more. It's a collection of best practices without seeming
like a dictionary. I could spend a few months practicing and studying
these best practices and I'd feel like I knew more about how to build
reliable systems.

The last half of the book is all about how to build communities and
processes that last. Building distributed systems requires effective
communication. There's a wealth of knowledge in the last portion of
the book about how to enable that. Some great advice includes:

- separate maintainers from contributors
- use Github (which can be interpreted as: make issue tracking,
  persistent comments, and code review *really* easy)

A good chunk of the community building advice is available as the ZMQ
[C4.1](http://rfc.zeromq.org/spec:22) guide. It's elaborated on much
more deeply in this book, which I appreciate. C4 is the contract; this
book is the rationale.

If you're interested in or need to build a system that's highly
concurrent, has high reliability and/or performance requirements, and
even if you can't use ZMQ to build said system, get this book
anyway. You'll learn a lot in the process that you can apply to making
network (and team) communication much more effective.
