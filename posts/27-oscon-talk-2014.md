-----
title: OSCON 2014 -  Speaking
date: July 22, 2014
tags: haskell, types, community
-----

Slides available at the bottom of this post.

I'll be speaking today at OSCON. It's been nearly 6 months since I
proposed [The Case for Haskell](/posts/oscon-2014-proposal.html), so
I'm all too excited to share what I've learned since then. Believe me,
there's **a lot** to be excited about!

However, there's a plot twist: the goals of my talk have changed and
so has the title. I'm now presenting *Ask More of Your Programming
Languages*. This blog post explains my rationale.

The first problem I started to encounter **in principle** is that by
its very nature, the title "The Case for Haskell" is exclusive. It
segregrates Haskell as a functional programming tribe where **all**
the good ideas are at. That's wrong and toxic. This is not the message
I want to share when I gush with excitement about functional
programming and type systems.

This leads into my next problem: the title is **too**
specific. There's a pun here about good design and leveraging
parametricity. Most of the ideas I'm sharing are applicable to all
functional programming languages with type systems. Through gradual
typing, a few more get added in! So instead of presenting:

```haskell
-- The Case for Haskell
awesome :: Haskell -> IO Conference
```

I'm now speaking about:

```haskell
-- Ask More of Your Languages
awesome :: FunctionalLang a , Typed a => a -> IO Conference
```

The implementation and the presentation are now generic, and help
bring great ideas closer together. This is more likely to pass my
personal community test suite.

One final problem that I noticed was that I think I was ruling out the
agency of the audience by pressuring a particular language or
ecosystem. I don't know the needs that others have. It that line, I
don't feel it's my place to criticize their language choices. We have
an incredible burden of legacy, and owning that is no small matter
that can be simply rewritten away.

In sum:

* The original talk encouraged an exclusive view point
* The original talk was more applicable than I allowed for
* It ignored the agency/needs of the audience by prescribing solutions

My hope is that I can share this excitement as **inclusively** as
possible. See you all at 5pm Pacific time!

[Slides](/files/ask-more-of-your-languages.pdf) (PDF)
