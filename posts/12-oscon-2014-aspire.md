-----
date: January 28, 2014
title: OSCON 2014 Aspirations -  The Case for Haskell
tags: oscon, haskell
-----

**Update (Feb. 01, 2014, 00:18am EST)**: I uploaded by proposal to OSCON
two days ago, then blogged about it. You can see the result here:
[OSCON 2014 Proposal](http://queertypes.com/posts/oscon-2014-proposal.html)

It's been an exciting year, and it's only just started.

Inspired by movements like
[Allowed to Apply](http://allowedtoapply.tumblr.com/) and following
all kinds of amazing people on Twitter, I'm going to submit an OSCON
proposal this year.

I decided this about a day and a half ago, a bit close to the proposal
submission deadline. I've had to move fast as a result.

**Yesterday**, I brainstormed on what I want to speak
on. Haskell. Definitely Haskell. It's what I've poured a good portion
of my free time in to over the past few months. Sadly, I've yet to
even say hello on #haskell, but it's been great listening in to
discussions on there!

So, more on that brainstorming -

The title: "**The Case for Haskell**" The track: Emerging Languages
The goals:

1. Instill excitement in Haskell
2. Show where Haskell can help you (a list of 10)
3. Share 5 places where Haskell is changing the way we think

There's a lot of pieces left to fill in there still.

Today, I put together a short summary of talks that emphasized how
language features helped people solve problems that were presented at
OSCON 2013. I'm sharing this guide with you today in the hope that it
helps you build a great submission to OSCON 2014.

You can find the guide here:
[OSCON 2013: What People Were Saying About Languages](https://dl.dropboxusercontent.com/u/61906905/audience.pdf)

Happy reading!
