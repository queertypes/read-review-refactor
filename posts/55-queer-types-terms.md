----------
date: October 17, 2015
title: Terms and Boundaries for my Works
tags: business, terms
----------

This is a short document highlighting what I can do, what I am willing
to do, and under what terms.

If you'd like to help me, please consider donating to my [Patreon](https://www.patreon.com/queertypes?ty=h).

## What I Can Do

I write Haskell, primarily. I've written quite a bit of it. You can
see a sampling of that work on my
[Gitlab](https://gitlab.com/u/queertypes). You may also turn to my
[resume](https://queertypes.com/files/resume.pdf) for additional
pointers.

I specialize in backend and systems work. I'm comfortable with
implementing components of projects, as well as full projects.

I can also teach, write, and draw. I use a combination of those skills
to communicate, express ideas, and make concepts more accessible.

I am an intersectional feminist. This means that my works try to take
into account the needs of all people, but especially those that are
being hurt by our current systems.

## Terms of my Works

* I will not work on secret or proprietary software
* I will not work on secret or proprietary research
* I will not patent my original works
* I will enforce a [code of conduct](http://contributor-covenant.org/) on my projects
* I will work on software projects available to all
* I will work on software with licensing approved by [OSI](<http://opensource.org/licenses>)
* I will release my own software under such licenses
* I will create other works under the [CC-BY](http://creativecommons.org/licenses/by/4.0/) license
* I may refuse to work on projects with toxic maintainers or communities
* Consent first: our terms must be mutually agreeable
* I expect to be paid for my labor
* None of the above is negotiable
