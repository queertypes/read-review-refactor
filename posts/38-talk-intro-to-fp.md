-----
title: Talk - Introduction to Functional Programming
date: October 27, 2014
tags: haskell, types, fp
-----

Slides: [pdf](/files/intro-to-fp.pdf), Repository: [gitlab](https://gitlab.com/queertypes/intro-to-fp/tree/master)

Today I'll be giving a talk introducing the notion of functional
programming. It tries to cover the fundamentals and to give listeners
a working knowledge of terminology and ideas used therein.

Some of the things I cover:

* Representative languages
* Expressions
* Immutability
* Functions
* Referential transparency
    * With definitions for effects, idempotency, and purity
* Types (errors, sums, products)
* Higher-order functions
    * map
    * filter
    * foldr
    * compose
* Challenges
* Making FP yours: breadcrumbs for the curious
* Additional resources

I used Haskell as a medium.

I hope these materials are useful to you, Happy viewing!
