-----
date: March 14, 2013
title: A Dry Spell, Some New Books
tags: books, review, clojure
-----

It's been awhile, readers. Sometimes things get so busy that finding
the time to write a few sentences seems like more than I can afford!
Here's a few quick points:

[Joy of Clojure 2e](http://joyofclojure.com/): I reviewed this while
it was still in early release format. It looks like a pretty solid
book. I feel like I would've gotten more out of it if I had more
experience with the JVM platform and Lisps overall.

New:

* [ZeroMQ](http://shop.oreilly.com/product/0636920026136.do), O'Reilly
  Media: It looks like a very promising read. The technology has been
  around for awhile, and I've seen it come up more than once in casual
  technical discussion. I grabbed a copy of the book and I'll be
  reading up on it soon.

* [Clojure Programming](http://shop.oreilly.com/product/0636920013754.do),
  O'Reilly Media: Lisps continue to elude me, and I thought I'd dive
  in with this O'Reilly publication. Having been published very
  recently, and having received rather high reviews, I'm excited to
  see what this will teach me, especially with how meta-programmable
  I've heard Lisps can be.

'til next time - happy reading!
