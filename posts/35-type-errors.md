-----
title: Comparing Type Error Messages Across Languages
date: October 5, 2014
tags: types, ux
-----

**Update** (12pm): Made chosen implementation of a language more
  clear, as error message quality varies across implementations.

The purpose of this post is to survey the landscape of error messages
from various language implementations with respect to a simple type
mismatch. I wanted to take a look at the many ways compilers and
interpreters respond to:

```haskell
1 + "1"
```

This is a rather trivial example, and that's part of the point. I
didn't want to bring in polymorphism, higher-kinds, or any nesting of
said features. I *did* want to higlight what languages would catch
this at compile-time, what languages would catch this at run-time, and
what languages would just coerce. In all cases, I wanted to see how
clearly the nature of the error was expressed.

Some languages fail to catch this error, so I gave them the following
test as a second chance:

```haskell
1 < "1"
```

## Good Error Messages

A good error message signals clearly and succinctly what went
wrong. The design of good error messages is demanding on system
designers. It requires that:

* There are few or no corner cases (sound, complete semantics)
* The system is well understood (abstraction <-> concretion)
* All of the above is communicated (effective communication)

Here's an example of what **not** to do:

![](/images/mapping-error.png)

With that out of the way, let's take an overview via language
categories and their representatives:

## Functional. Compile-time Type Check {#static-fun}

Functional programming languages tend to provide the most feature-rich
type systems. Notably, only one language below fails test (1)
above. That same language passes test (2).

* [agda](/posts/type-errors.html#agda) (1)
* [ats](/posts/type-errors.html#ats)
* [coq](/posts/type-errors.html#coq) (1)
* [fsharp](/posts/type-errors.html#fsharp)
* [ghc/haskell](/posts/type-errors.html#haskell)
* [idris](/posts/type-errors.html#idris)
* [ocaml](/posts/type-errors.html#ocaml)
* [rust](/posts/type-errors.html#rust)
* [scala](/posts/type-errors.html#scala)
* [sml/nj](/posts/type-errors.html#sml)
* [swift](/posts/type-errors.html#swift)

Agda and Coq get special tests because they don't package ints and
strings as primitive types, as far as I can tell. They certainly would
pass tests (1) and (2) above, though.

## Functional, Run-time Type Check {#rt-fun}

These languages give users the option of leveraging type checking
before running their programs. For example, check out
[Typed Clojure](https://www.youtube.com/watch?v=a0gT0syAXsY) at
Strange Loop 2014. All of these languages catch the type mismatch
above.

* [clojure](/posts/type-errors.html#clojure)
* [common lisp](/posts/type-errors.html#sbcl) (sbcl)
* [emacs lisp](/posts/type-errors.html#elisp) (elisp)
* [elixir](/posts/type-errors.html#elixir)
* [erlang](/posts/type-errors.html#erlang)
* [racket](/posts/type-errors.html#racket)

## Object-oriented, Compile-time Type Check {#static-oo}

Object-oriented languages tend to discard type-safety in favor of more
*familiar* [^1] models of computation. Two of the five languages below
fail type test (1). All of them pass type test (2).

* [csharp](/posts/type-errors.html#csharp)
* [clang/c++](/posts/type-errors.html#cplusplus)
* [d](/posts/type-errors.html#d)
* [go](/posts/type-errors.html#go)
* [sun/java](/posts/type-errors.html#java)

## Object-oriented, Run-time Type Check {#rt-oo}

This set of languages amounts for most of the code written
today. Three of these seven fail type test (1). Two of them fail even
type test (2). Python ([mypy](http://www.mypy-lang.org/)), Ruby
([rtc](https://github.com/plum-umd/rtc)), PHP
([hack](http://hacklang.org/)), and Lua
([typed Lua](https://github.com/andremm/typedlua)) all appeared within
the past year to support optional, compile-time type checking.

* [lua](/posts/type-errors.html#lua)
* [perl](/posts/type-errors.html#perl)
* [php / hack](/posts/type-errors.html#php)
* [c/python](/posts/type-errors.html#python)
* [mri/ruby](/posts/type-errors.html#ruby)
* [R](/posts/type-errors.html#R)

## Imperative {#imp}

These languages operate close to the model of hardware and tend to
have poor support for abstraction.

* [gnat/ada](/posts/type-errors.html#ada)
* [clang/c](/posts/type-errors.html#c)
* [g/fortran](/posts/type-errors.html#fortran)

## Javascript {#jslangs}

Powers your browser, for better or worse. Fortunately, all of the
alternatives fare better than vanilla JS in the type test.

* [javascript](/posts/type-errors.html#javascript)
* [dart](/posts/type-errors.html#dart)
* [elm](/posts/type-errors.html#elm)
* [purescript](/posts/type-errors.html#purescript)
* [typescript](/posts/type-errors.html#typescript)

## SQL {#sql}

Finally, why not? How do these SQL implementations handle a variation
on the type test?

* [mysql](/posts/type-errors.html#mysql)
* [sqlite](/posts/type-errors.html#sqlite)

## Other {#other}

I've included a few applied-math-oriented languages below. Octave is a
matrix/vector DSL of sorts. R is a statistics-focused programming
environment.

* [octave](/posts/type-errors.html#octave)
* [R](/posts/type-errors.html#R)

## Wrap Up

We've seen a variety of error messages and language styles. Some
languages caught errors at compile-time, others at run-time. Some
languages lacked a REPL. Some languages did not pass test (1). Some,
even failed test (2). Some languages required some boilerplate code to
run these tests. Some languages required entirely different tests.

This experiment was greatly facilitated by
[nix](http://nixos.org/nix/). Often times, it was just a matter of:

```bash
$ nix-env -i <language>
```

... to get up and running!

An interesting follow up would be to see how languages supporting
polymorphism with compile-time type checking express type errors
involving polymorphism.

Towards what end did I write this post? Curiosity! Programming
languages are interesting in and of themselves to me. I hope that my
curiosity has given you some entertaining reading!

If nothing else, this post demonstrates the variety of programming
languages that exist. The world is rich with means to program
computers. I encourage you to explore it!

## The Catalogue

### Agda [↵](/posts/type-errors.html#static-fun) {#agda}

```agda
data Bool : Set where
  true : Bool
  false : Bool

data Nat : Set where
  zero : Nat
  succ : Nat -> Nat

not : Bool -> Bool
not true = zero
not false = true
```

```agda
/home/allele/langs/x.agda:23,12-16
Nat !=< Bool of type Set
when checking that the expression zero has type Bool
```

### ATS [↵](/posts/type-errors.html#static-fun) {#ats}

```ocaml
val _ = 1 + "1"
```

```bash
$ atscc x.dats
/home/allele/x.dats: 9(line=1, offs=9) -- 16(line=1, offs=16): error(3): the symbol [+] cannot be resolved: there is no match.
exit(ATS): uncaught exception: ATS_2d0_2e2_2e11_2src_2ats_error_2esats__FatalErrorException(1027)
```

### Coq [↵](/posts/type-errors.html#static-fun) {#coq}

```haskell
Inductive day : Type :=
  | monday : day
  | tuesday : day
  | wednesday : day
  | thursday : day
  | friday : day
  | saturday : day
  | sunday : day.

Inductive bool : Type :=
  | true : bool
  | false : bool.

Definition negb (b:bool) : bool :=
  match b with
    | true => monday
    | false => true
  end.
```

```
Toplevel input, characters 64-70:
Error: In environment
b : bool
The term "monday" has type "day" while it is expected to have type
"bool".
```

### Fsharp  [↵](/posts/type-errors.html#static-fun) {#fsharp}

```fsharp
1 + "1";;

  1 + "1";;
  ----^^^

/home/allele/stdin(1,5): error FS0001: The type 'string' does not match the type 'int'
```

### Haskell [↵](/posts/type-errors.html#static-fun) {#haskell}

```haskell
1 + "1"

    No instance for (Num [Char]) arising from a use of ‘+’
    In the expression: 1 + "1"
    In an equation for ‘it’: it = 1 + "1"
```

### Idris [↵](/posts/type-errors.html#static-fun) {#idris}

```idris
1 + "1"
Can't resolve type class Num String
```

### Ocaml [↵](/posts/type-errors.html#static-fun) {#ocaml}

```ocaml
1 + "1";;
Error: This expression has type string but an expression was expected of type
         int
```

### Rust [↵](/posts/type-errors.html#static-fun) {#rust}

```rust
fn main() {
  return 1 + "1";
}
```

```rust
$ rustc x.rs
x.rs:2:14: 2:17 error: mismatched types: expected `<generic integer #0>`, found `&'static str` (expected integral variable, found &-ptr)
x.rs:2   return 1 + "1";
                    ^~~
x.rs:2:10: 2:17 error: mismatched types: expected `()`, found `<generic integer #0>` (expected (), found integral variable)
x.rs:2   return 1 + "1";
                ^~~~~~~
error: aborting due to 2 previous errors
```

### Scala [↵](/posts/type-errors.html#static-fun) {#scala}

```scala
1 + "1"
"11"
1 < "1"
error: overloaded method value < with alternatives:
  (x: Double)Boolean <and>
  (x: Float)Boolean <and>
  (x: Long)Boolean <and>
  (x: Int)Boolean <and>
  (x: Char)Boolean <and>
  (x: Short)Boolean <and>
  (x: Byte)Boolean
 cannot be applied to (String)
              1 < "a"
```

### Standard ML (NJ) [↵](/posts/type-errors.html#static-fun) {#sml}

```ocaml
1 + "1";
stdIn:1.2-1.9 Error: operator and operand don't agree [literal]
  operator domain: int * int
  operand:         int * string
  in expression:
    1 + "1"
```

### Swift [↵](/posts/type-errors.html#static-fun) {#swift}

```haskell
1 + "1"

error: type 'Int' does not conform to protocol 'ExtendedGraphemeClusterLiteralConvertible'
```

---

### Clojure [↵](/posts/type-errors.html#rt-fun) {#clojure}

```clojure
(+ 1 "1")

ClassCastException java.lang.String cannot be cast to java.lang.Number  clojure.lang.Numbers.add (Numbers.java:126)
```

### Common Lisp (SBCL) [↵](/posts/type-errors.html#rt-fun) {#sbcl}

```commonlisp
(+ 1 "1")

debugger invoked on a SIMPLE-type-errors in thread
#<THREAD "main thread" RUNNING {1002D6D1F3}>:
  Argument Y is not a NUMBER: "1"

Type HELP for debugger help, or (SB-EXT:EXIT) to exit from SBCL.

restarts (invokable by number or by possibly-abbreviated name):
  0: [ABORT] Exit debugger, returning to top level.

(SB-KERNEL:TWO-ARG-+ 1 "1")
```

### Emacs Lisp [↵](/posts/type-errors.html#rt-fun) {#elisp}

```commonlisp
(+ 1 "1")

1 Debugger entered--Lisp error: (wrong-type-argument number-or-marker-p "1")
  2   +(1 "1")
  eval((+ 1 "1") nil)
  eval-last-sexp-1(nil)
  eval-last-sexp(nil)
  funcall-interactively(eval-last-sexp nil)
  call-interactively(eval-last-sexp nil nil)
  command-execute(eval-last-sexp)
```

### Elixir [↵](/posts/type-errors.html#rt-fun) {#elixir}

```elixir
1 + "1"
```

```elixir
$ elixirc x.ex

x.ex:1: warning: this expression will fail with a 'badarith' exception

== Compilation error on file x.ex ==
** (ArithmeticError) bad argument in arithmetic expression
    x.ex:1: (file)
    (elixir) src/elixir_lexical.erl:17: :elixir_lexical.run/3
    (elixir) lib/kernel/parallel_compiler.ex:95: anonymous fn/4 in Kernel.ParallelCompiler.spawn_compilers/8
```

### Erlang [↵](/posts/type-errors.html#rt-fun) {#erlang}

```erlang
1 + "1".
** exception error: an error occurred when evaluating an arithmetic expression
     in operator  +/2
        called as 1 + "1"
```

### Racket [↵](/posts/type-errors.html#rt-fun) {#racket}

```scheme
(+ 1 "1")
; +: contract violation
;   expected: number?
;   given: "1"
;   argument position: 2nd
; [,bt for context]
```

---

### C# [↵](/posts/type-errors.html#static-oo) {#csharp}

```c
1 + "1"
"11"
1 < "1"
(1,2): error CS0019: Operator `<' cannot be applied to operands of type `int' and `string'
```

### C++ [↵](/posts/type-errors.html#static-oo) {#cplusplus}

```c
int main()
{
  return 1 + "1";
}
```

```c
x.c:3:10: error: cannot initialize return object of type 'int' with an rvalue
      of type 'const char *'
  return 1 + "1";
         ^~~~~~~
1 error generated.
```

### D [↵](/posts/type-errors.html#static-oo) {#d}

```d
int main() {
  return 1 + "1";
}
```

```d
$ dmd x.d
x.d(2): Error: incompatible types for ((1) + ("1")): 'int' and 'string'
```

### Go [↵](/posts/type-errors.html#static-oo) {#go}

```go
package main

func main () {
  return 1 + "1";
}
```

```go
$ go build x.go
./x.go:4: cannot convert "1" to type int
./x.go:4: invalid operation: 1 + "1" (mismatched types int and string)
./x.go:4: too many arguments to return
```

### Java [↵](/posts/type-errors.html#static-oo) {#java}

```java
class App
{
  public static void main()
  {
    return 1 < "1";  // gladly accepts 1 + "1"
  }
}
```

```java
$ javac x.java
x.java:5: error: incompatible types: unexpected return value
    return 1 < "1";
             ^
1 error
```

---

### Lua [↵](/posts/type-errors.html#rt-oo) {#lua}

```lua
print(1 + "1")
2
print(1 < "1")
stdin:1: attempt to compare number with string
stack traceback:
	stdin:1: in main chunk
	[C]: in ?
```

### Perl [↵](/posts/type-errors.html#rt-oo) {#perl}

```perl
print 1 + "1"
print 1 < "1"
```

```perl
$ perl x.pl
2
```

### Hack, PHP [↵](/posts/type-errors.html#rt-oo) {#php}

```php
<?hh
function f(int $x): int {return $x+"cat";}

echo f(1);
echo f("cat");
```

```php
$ hhvm x.php
1
Fatal error: Argument 1 passed to f() must be an instance of int, string given in /home/allele/x.php on line 2
```

```php
<?php
function f($x) {return $x+"cat";}

echo f(1);
echo f("cat");
```

```php
$ php x.php
10
```

### Python [↵](/posts/type-errors.html#rt-oo) {#python}

```python
1 + "1"
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for +: 'int' and 'str'
```

### Ruby [↵](/posts/type-errors.html#rt-oo) {#ruby}

```ruby
1 + "1"
TypeError: String can't be coerced into Fixnum
	from (irb):1:in `+'
	from (irb):1
	from /usr/bin/irb:12:in `<main>'
```

---

### Ada [↵](/posts/type-errors.html#imp) {#ada}

```ada
with Ada.Text_IO; use Ada.Text_IO;
procedure X is
begin
  Put_Line(1 + "1");
end X;
```

```ada
$ gnatmake x.adb
gcc-4.6 -c x.adb
x.adb:4:16: expected type universal integer
x.adb:4:16: found a string type
gnatmake: "x.adb" compilation error
```

### C [↵](/posts/type-errors.html#imp) {#c}

```c
int main()
{
  return 1 + "1";
}
```

```c
$ clang x.c
x.c:3:10: warning: incompatible pointer to integer conversion returning
      'char *' from a function with result type 'int' [-Wint-conversion]
  return 1 + "1";
         ^~~~~~~
1 warning generated.
```

### Fortran [↵](/posts/type-errors.html#imp) {#fortran}

```fortran
      program hello
        print *, 1 + "1"
      end program hello
```

```fortran
$ gfortran x.f
x.f:2.16:

        print *, 1 + "1"
                1
Error: Operands of binary numeric operator '+' at (1) are INTEGER(4)/CHARACTER(1)
```

---

### Javascript [↵](/posts/type-errors.html#jslangs) {#javascript}

```javascript
1 + "1"
'11'
```

### Dart [↵](/posts/type-errors.html#jslangs) {#dart}

```javascript
int main() {
  return 1 + "1";
}
```

```javascript
$ dart2js x.dart
Warning: 'String' is not assignable to 'num'.
  return 1 + "1";
             ^^^
../lib/core/num.dart:83:16:
Info: This is the method declaration.
  num operator +(num other);
```

### Elm [↵](/posts/type-errors.html#jslangs) {#elm}

```haskell
main = 1 + "1"
```

```haskell
$ elm x.elm
[1 of 1] Compiling Main                ( x.elm )
Type error on line 1, column 8 to 15:
A number must be an Int or Float.
        1 + "1"

   Expected Type: number
     Actual Type: String
```

### Purescript [↵](/posts/type-errors.html#jslangs) {#purescript}

```haskell
1 + "1"

Error at  line 1, column 1:
Error in declaration it
Error in expression "1":
Expr does not have type Prim.Number
```

### Typescript [↵](/posts/type-errors.html#jslangs) {#typescript}

```javascript
function a(x: string): void {}

a(1);

Supplied parameters do not match any signature of call target: Could not apply 'string' to argument 1 which is of type 'number'.
```

---

### MySQL [↵](/posts/type-errors.html#sql) {#mysql}

```sql
> select abs(-1);
+---------+
| abs(-1) |
+---------+
|       1 |
+---------+
1 row in set (0.00 sec)

> select abs("cat");
+------------+
| abs("cat") |
+------------+
|          0 |
+------------+
1 row in set, 1 warning (0.00 sec)
```

### sqlite [↵](/posts/type-errors.html#sql) {#sqlite}

```sql
select abs("cat");
0.0
```

---

### Octave [↵](/posts/type-errors.html#other) {#octave}

```octave
1 + "1"
> 50
```

### R [↵](/posts/type-errors.html#other) {#R}

```R
1 + "1"
Error in "1" + "1" : non-numeric argument to binary operator
```

---

[^1]: Object-orientation is familiar at this point because it is so
widely taught. It is uncommon to find programming courses, academic or
otherwise, that teach alternate models of expression.
