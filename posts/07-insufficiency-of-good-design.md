-----
title: Sarah Mei. The Insufficiency of Good Design. Ruby Conf 2012.
date: July 22, 2013
tags: communication, talk
-----

[Link](https://www.youtube.com/watch?v=UgrVdHYEZGg)

Watch this at some point. If you work with people, even if you're not
in the software development industry, you'll benefit from the ideas
shared in this video.

I learned about this video today from a colleague. It turns out
there's a lot of wisdom in it. It actually surprised me quite a few
times. According to the video, which in turn cites a Carnegie Mellon
University study conducted in 2007, the best predictor of quality in
code is...

**Good Communication**

This trumps technical qualifications and domain-specific knowledge. I
tried hard to find the exact paper Sarah spoke of, but the closest I
could come to that study was an article published in 1999. As a side
note, many of the papers listed on the parent page are very
interesting from an organizational point of view. *bookmarked*

Before I go further, allow me to quote Conway's Law:

> Any organization that designs a system will inevitably produce a
> design whose structure is a copy of the organization's communication
> structure.  Studies seem to indicate that this is real, real as in
> take a look at projects you've worked on and see for yourself real.

So given that Conway's Law is something that is affecting you right
now, it means that you can actually debug your communication
patterns. This was one of the surprising points Sarah brought up. It
turns out that by reflecting on code that's been written, and "code
smells" that keep coming up, you can determine missing links in
communication. You could continue to solve those code smells. You
could refactor them all away - once per sprint even! However, that's
only addressing the symptoms. The underlying problem is more likely to
be one of communication, and if you address that, it should follow
that the anti-pattern in the code should go away. A lot of conjecture
here - take home experiments?

To sum it up: Every piece of bad code is trying to tell you something.

It's up to you to listen.
