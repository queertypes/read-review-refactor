-----
title: On Beginner Languages, Choosing, and Tribalism
date: December 22, 2014
tags: social, programming
-----

In several
[Twitter](https://twitter.com/jonathanmarvens/status/546726686717652992)
discussions today, I saw heated arguments happening, which at best,
were attempting to deconstruct the technical factors behind the choice
of languages a beginner should be exposed to. What should beginners
learn? Where should a beginner get their start in programming?

People were getting defensive about their favorite languages being
called out for shortcomings, as they do. It happens too often, and
this sort of thing puts people off from (at the very least) getting
**publicly** involved with languages they're curious about. It doesn't
help. It's a hostile space where people are trying to sell their story
of "what's right" and invoking the magic framing of
"anti-intellectualism" or "pragmatism" (sometimes both at once!) when
their views aren't adopted. This. Doesn't. Help.

[Python](https://www.python.org/),
[Ruby](https://www.ruby-lang.org/en/),
[Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript). [Erlang](http://www.erlang.org/),
[Haskell](https://www.haskell.org/haskellwiki/Haskell),
[Ocaml](http://ocaml.org/). [Go](http://golang.org/),
[Java](http://www.oracle.com/technetwork/java/index.html),
[C](http://en.wikipedia.org/wiki/C_%28programming_language%29). There's
a whole cornucopia of languages to choose from, each with varying
degrees of freely available resources, varying degrees of
accessibility to beginners, and even varying degrees of uptake in
industry.

In all these discussions, what I **did not** see are consideration for
the social factors that determine what a beginner can
choose. **Privilege**. Particularly:

1) Not everyone has the opportunity (or will) to go to college
2) Not everyone has access to mentors, friends, or community
3) Not every language has the same probability of resulting in a paycheck

More important than the technical factors, is being able to choose a
language that will allow one to **live long enough** and build a base
from which to further progress. That's a hard call to make, and it's
not made easier by a culture that punishes for "making the wrong
choice".

We live in a mountain of legacy software, and as we are now, as a
society, with our current economic systems, it is valuable to have the
tools to begin to chip away at those legacy arrangements. In the
browser, this will frequently mean Javascript, for all its woes. In
the web development space, a terrifying mix of Rails and Ruby (1.8
even).

Once one has stable income and can take risks, it's fair to take some
time to delve deeply into things like Haskell. That's what I did. I
worked nearly two years in industry, with a focus on Python and C++. I
built up enough funds so I could at least *risk* six months focusing
on Haskell, whether or not employment came my way (it did, and I moved
to Austin, TX). I even had a short stint of doing Haskell
professionally. I was only able to do this because I planned for it,
and be safe enough in that pursuit.

I came from a background that wasn't great financially. I **did** get
to go to college (Masters, C.S.), so I do ride on that privilege. I
got plenty of Linux/C/C++ experience while working through that
degree, so the command line wasn't a barrier for me. There's so much
to learn when it comes to computers. I **still** suffered intense
impostor syndrome when I first started to navigate the Haskell space.

With all that said, back on point - please, can we consider what the
beginner's background is before getting defensive about what we should
be teaching them?

Yes, it's **wonderful** to be able to learn and work with Haskell and
other functional languages. Yes, they're even starting to gain more
mindshare in industry. Still, even with the right teachers, even with
the right books and resources, even with the right community, it still
might not be the right **first** choice for everyone. That's for an
individual to decide with the information and resources they have at
hand. Please keep that in mind.

Only an individual can choose for themselves what they want or need to
learn, and the best we can do to help them on that path from beginner
to mastery is to help them navigate this mess of tribal propaganda and
help them find what they need. Reduce the noise, boost the signal, and
listen, listen, listen.
