-----
title: Digest - Socio-PLT -  Principles for Programming Language Adoption
date: August 27, 2014
tags: haskell, research, social
-----

Link: [pdf](http://parlab.eecs.berkeley.edu/sites/all/parlab/files/Socio-PLT.pdf)

This paper caught my eye today since I've been doing some comparative
analysis of programming languages. In sum, it analyzes adoption of
programming languages. It poses very insightful questions and proposes
hypotheses for future exploration.

Some salient aspects for the functional programming community and type
system researchers are brought up. Module systems, types, and FP are
all touched upon, and Haskell is named a few times.

I quote the questions and hypotheses below for easy perusing. I'll
follow up with some of my thoughts and experiences.

---

## Synopsis: Questions

1. Why are industrial language designers adopting generators and
   coroutines instead of continuations?

2. Was Backus right in that language complexity is a sufficiently
   persuasive reason to adopt functional programming? More strongly,
   is worse better[^1] in that implementation simplicity trumps
   language generality?

3. What actually convinces programmers to adopt a language?

4. How can we prevent sexy[^2] types from being another source of
   "prolonged embarrassment" to the research community?

5. What data should language designers track?

6. How do we perform, evaluate, and benefit from research into
   developing applications in a language?

7. What are the perceptions about testing and typing that drive the
   differing beliefs, how do they relate to practice, and what
   opportunities and course corrections does this pose?

8. What is an appropriate model for adoption?

9. How can we exploit social networks to persuade language
   implementors and programmers to adopt best practices?

10. How can we find and optimize for domain boundaries?

11. How many languages do programmers strongly and weakly know? Is
    there a notion of linguistic saturation that limits reasonable
    expectations of programmers?

12. How can feature designers more directly observe and exploit social
    learning?

13. How can researchers, language implementors, and programmers
    cooperate to expedite adaptation?

14. Has controversy restricted the design space for programming
    language techniques?

15. What sorts of programmers are early adopters of new languages and
    tools? What features and languages are they familiar with?

16. How adverse are programmers to longer compilation or interpreter
    startup times? How willing are they to trade time for improved
    error checking?

17. How does latency sensitivity vary across user populations?

18. Can we ease evaluation of proposed language features? Can we
    catalog and predict the likely sources of trouble?

19. Which other programming language features (aside from modularity)
    also require socio-technical evaluation?

20. To what extent do distinct language communities have distinct
    values? Are there values that are important to one community and
    completely irrelevant to another?

21. How do the values of a language community change over time? For
    instance, do designers become more or less performance-focused as
    languages become popular? More or less focused on ease of
    implementation?

## Synopsis: Hypotheses

1. Working on the closure side of the duality increases influence on
   programming language researchers, but decreases influence on both
   practicioners and software engineering researchers.

2. Both programming language designers and programmers incorrectly
   perceive the perfromance of languages and features in practice.

3. Developer demographics influence technical analysis.

4. Programmers will abandon a language if it is not updated to address
   use cases that are facilitated by its competitors.

5. The productivity and correctness benefits of functional programming
   are better correlated with the current community of developers than
   with the languages themselves.

6. The diffusion of innovation model better predicts language and
   feature adoption than both the diffusion of information model and
   the change function.

7. Many languages and features have poor simplicity, trialability, and
   observability. These weaknesses are more likely in innovations with
   low adoption.

8. A significant percentage of professional programmers are aware of
   functional and parallel programming but do now use them. Knowledge
   is not the adoption barrier.

9. Developers primarily care about how quickly they get feedback about
   mistakes, not about how long before they have an executable binary.

10. Users are more likely to adopt an embedded DSL than a non-embedded
    DSL and the harmony of the DSL with the embedding environment
    further increases the likelihood of adoption.

11. Particular presentations of features such as a
    language-as-a-library improve the likelihood of short-term and
    long-term feature adoption.

12. Implementing an input-output library is a good way to test the
    expressive power and functionality of a language.

13. Open-source code bases are often representative of the complete
    universe of users.

14. Most users are tolerant of compilers and interpreters that report
    back anonymized statistics about program attributes.

15. Many programming language features such as modularity mechanisms
    are tied to their social use.

---

## My Thoughts

### On compilation time and interpreter startup time

First, there must be an interpreted mode for a language. It's been
extremely valuable to me to be able to hop into a REPL to experiment
with a language feature. This was especially valuable to me when I was
first learning Python, and afterwards, when I started learning
Haskell. With Haskell in particular, I found it easier once
understanding how the type system worked to prototype at the type
level working with ghc-mod and emacs.

This brings me to my next point, addressing hypothesis (9) - I believe
this to be true generally, and I'm certain that it is true for me
personally. Towards this end, I advocate for languages to decouple
their type-checking phase from the remainder of their compilation
pipeline. All the better if this is exposed as a compiler-mode or even
a library. I've used this to great effect while working on Haskell
code with emacs. Knowing whether what I've written will even build is
often just a save away. It takes less than a second (and often much
closer to less than 100ms) to get rich feedback. By rich, I mean,
errors are highlighted, and if I hover over a particular line, I can
see the cause of the error in the form of a compiler type error
message or as a linter suggestion.

![](/images/haskell-error.jpg)

I can speak to hypothesis (10) a bit. Libraries like [Parsec]() and
[Cryptol]() in Haskell make a strong case for this concept. Parsec
provides a potent, eDSL for parsing arbitrary data. Constructing an
efficient parser using this language is surprisingly
pleasant. Something like Cryptol goes a step further even, challenging
what I believed was possible with Haskell. It is an eDSL for
specifying cryptographic algorithsm which in turn generates provably
correct C code. There is much merit to the proliferation of harmonious
eDSLs.

Hypothesis (14) makes me nervous. I would be okay submitting
anonymized statistics. **However**, this choice must be clearly
presented to the users, and their consent explicitly given.

I submit another question that may have not been considered.

22. How does the presence of legacy software and it's maintenance
    requirements affect users ability to choose new languages and
    techniques?

From what I've seen in my short time working in industry, this is a
very real problem. I'm sensitive to this group of individuals because
I've spent some time assisting in these scenarios. A rewrite isn't
always possible, unfortunately.

## Closing Thoughts

I'm happy to see social considerations being explored in the
programming languages space. It's a problem that's dear to me, since I
spend a bit of my time advocating for the adoption of empowering type
systems and functional techniques. I think I'll sit with this paper
for some time and see how it changes how I look at the process of
advocacy.

## Footnotes

[^1]:
R. Gabriel.
[The rise of “worse is better”. Lisp: Good News, Bad News, How to Win Big](http://www.jwz.org/doc/worse-is-better.html), 1991.

[^2]: The description of a type system as a whole or in part as sexy
is unnecessary and potentially harmful. I'm aware of part of the
origin of this phrasing. "Empowering", "enabling", "intriguing" avoid
a slew of issues that "sexy" carries and express the idea more
clearly. A fair topic for another time.
