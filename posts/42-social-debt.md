-----
title: Social Debt
tags: haskell, software, management, social
date: December 21, 2014
-----

This will be a brain dump of sorts.

I've done a few deep dives this year, technically and socially. What I
want to write about right now is about one of the things I've
discovered at the intersection of these two areas.

On the tech side, I spent most of this year digging into the deepest
depths of Haskell, and languages like it. Type systems, algebras,
category theory, compiler extensions, pattern matching, and more. It's
been a hell of a time, and I've felt more capable of creating software
and understanding it this year than ever before. I mean it - software
in general. Whatever the language, whatever the domain, I feel like I
now have some incredibly potent tools to reason with. It's amazing!

On the social side, I discovered feminism, gender, and intersectional
analysis. I'll write more on that at a later time, but suffice it to
say, it was an awakening and enlightenment for me as an individual and
as a human in much the way that Haskell was for my development as a
person who works with software.

With all that said, I've realized that the social factors in a
software project are **as important if not more so** than the
technical factors. This includes things like:

* Code of conduct with experienced handlers/moderators
* Collective knowledge: languages, tools, techniques
* Governance: issue tracking, minutes, vision, tasks

On collective knowledge, - as this one is particularly relevant to my
experiences - it is important to choose technology in the present
based on what your current team can do. This doesn't mean to mandate a
particular set of tools. It also doesn't mean that the **now** has to
become the **forever**. Collective knowledge is the mesh of what
everyone who is working on the project knows. This can be grown over
time via shared experiences, via teaching, via review.

Projects that don't tend to their social factors accumulate **social
debt**. Technical debt speaks to technical architecture: test
coverage, type coverage, bottlenecks, design issues. Social debt is an
analogue to that: single points of failure with team member knowledge,
increasingly hostile interactions, implicitly-formed
[power structures](http://www.jofreeman.com/joreen/tyranny.htm), loss
of visibility on issue and task management.

Back to knowledge management - I think about this a lot, as a
Haskeller. Haskell, as a language, has been a barrier to participation
in a few projects I've worked on. It made it so that if the Haskellers
present on the project stepped away, and couldn't dedicate
teaching/leading time, the project would grind to a halt. This is not
an argument against specialization, I think. I think it means that
collective knowledge is something that needs to be tracked and managed
well to succeed. That's a hard problem!

So I guess that's it. I wanted to write about social debt in technical
projects. I don't have much in the way of answers. I'll just keep
thinking on it.
