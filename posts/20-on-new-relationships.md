-----
date: May 3, 2014
title: On New Relationships
tags: polyamory, me
-----

I've recently connected with some wonderful people. Going through this
process anew, I'm reminded of the challenges and excitements that come
with growing new relationships.

New relationship energy (NRE) hits me hard every time. By NRE, I mean
that I find myself thinking about this new person often throughout the
day, that emotions are felt more acutely and intensely. What would
normally be a discussion about cupcake flavors turns into a **rave**
about cupcake flavors. What might normally be a subtle commentary on
past events turns into an intense sharing of life experiences. Holding
hands or hugging, with [Cloverlimes](http://climeslover.tumblr.com/)
might feel sweet and warm, but becomes magical in the presence of NRE.

Balancing NRE is a tricky thing for me. It has lead me to poor
decisions in the past. I've learned over the past two years how to
process NRE more safely. I've found a few things that really
help. First, I take time to process. I revisit all my fundamental
beliefs and check that I'm not violating them by following through
with decisions that might have been spurred on by NRE. Also, I talk
about feelings. A lot.

Expressing the kinds of feelings I'm developing has helped me a
lot. For one, I feel more comfortable with people I can discuss
feelings comfortably with. This helps keep my worries in check, helps
me understand where boundaries lie, and helps me understand where a
relationship stands with regards to others I'm engaging with. It's
important for me to build trust, that I can do this.

With NRE safely in check (for the most part), there are some things
that are very fun about it. That extra energy does leak into
everything else that I do. Spending time with
[Cloverlimes](http://climeslover.tumblr.com/), I feel and act just a
little bit more giddy. I blush more. I process more. I smile more. I
feel more energized about sharing the things that I love in
general. I'm also reminded of things that are precious to me in my
current relationship, that I sometimes take for
[granted](http://kimchicuddles.com/tagged/chrono/chrono/page/310). Holding
hands, hugging, sleeping together, great emotional support, cooking
together, eating together. I feel an immense gratitude towards
Cloverlimes, and I'm all kinds of excited for experiencing all of
these things with new loves.

So yes, there's a lot going on at the moment. It's exciting! I've been
writing many letters, talking a lot, sharing phone calls, and
thinking. Just thinking. Possibilities, hopes, cautions.

I'm reminded of how important communication and love are to me. I stop
and wonder: Why aren't such things talked about and taught with more
priority?
