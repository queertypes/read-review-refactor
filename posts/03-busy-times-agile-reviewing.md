-----
title: Busy Times, Agile Reviewing
date: February 23, 2013
tags: agile, review, books
-----

Though I promised a review of
[Hackers and Painters](http://shop.oreilly.com/product/9780596006624.do)
a week ago, it's been busier than I anticipated.

I'll say this much - the last few chapters of
[Hackers and Painters](http://shop.oreilly.com/product/9780596006624.do)
are very interesting. They'll make you question how much of your
systems and applications you develop in low-level, compiled languages
and how much more you could be getting done if you used a more
powerful language. For aspiring system designers, these chapters are a
very worthwhile read.

Interesting side note: I wonder what Agile book reviewing would be
like? Perhaps I'll adapt that methodology instead of reviewing all at
once, with a retrospective at the end of each book that summarizes the
findings.
