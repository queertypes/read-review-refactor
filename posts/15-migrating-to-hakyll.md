-----
date: March 31, 2014
title: Migrating to Static, Self-Hosted Blog
tags: haskell, meta, hakyll
-----

Hello, readers.

Soon, I'll be moving away from the Blogger platform to a statically
hosted blog. I've been playing with
[hakyll](http://jaspervdj.be/hakyll/) for some time now, and am
pleased with it's support for syntax highlighting, markup formats,
RSS/Atom generation, and packaged Warp.

It'll be great to develop my blog with emacs, leveraging git for
backup and rsync for deployment.

Expect:

* Less latency
* A home-grown design
* More code samples, with pretty highlighting

With possibly:

* More frequent posts

Stay tuned. I'll be releasing the new blog by the end of the week!
