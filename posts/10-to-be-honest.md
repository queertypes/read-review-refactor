-----
date: December 14, 2013
title: To Be Honest...
tags: meta, thoughtstreams
-----

I had forgotten that I ramped up this blog. It's been so long since
I've looked at it, and I only came across it again since I've been
actively considering writing. I was surprised to see that people were
still periodically coming here over the past few months, even though
it's been nearly six months since I last posted (thanks!).

I've been spending more time publishing mini-blogs over at
[ThoughtStreams](https://thoughtstreams.io/). I have three categories
so far:

* [Marconi Updates](https://thoughtstreams.io/acabrera/marconi-progress-and-updates/)
* [Haskell](https://thoughtstreams.io/acabrera/haskell/)
* [Philosophy Behind Software, Love, and Life](https://thoughtstreams.io/acabrera/philosophy-behind-software-life-and-love/)

Thoughtstreams is a very interesting platform. More than any other
blogging engine I've ever used, it tries not to get in the way. I just
write, as little or as much as I like, and post. It even supports
Markdown syntax, with basic support for code blocks! I've felt more
compelled to write a little every day using that. It helps!

Marconi Updates is my micro-blog (shared with
[flaper87](https://thoughtstreams.io/flaper87/marconi-progress-and-updates/)
and
[kgriffs](https://thoughtstreams.io/kgriffs/marconi-progress-and-updates). Herein,
I write about our efforts on the
[Openstack Marconi](https://wiki.openstack.org/wiki/Marconi) project,
a distributed queuing system written entirely in Python. Most of my
updates are storage-centric, since that's one of the more interesting
aspects of the project to me.

Haskell is all about Haskell! I've developed a sort of infatuation
with the language over the past three years, even though I've yet to
use it develop personal or professional projects. The appeal of a
functional (double-meaning there), statically-typed language with
elegant, terse syntax really gets the language geek in me going. I've
found this channel to be a great place to gush about the language,
tidbits I find interesting, and what I hope to do with it.

Philosophy Behind Software, Love, and Life is a new experiment, shared
with
[flaper87](https://thoughtstreams.io/flaper87/philosophy-behind-software-life-and-love/). In
this microblog, I want to share more personal thoughts and
beliefs. Thus far, I've written about mindfulness and about some of
the personal struggles I've encountered in working with technology. I
hope to share more of my personal story therein, and how they guide my
beliefs on my journey through this world.

So that's the story. I think I isolated this blog to be too much about
reviewing books in the past, and that really damaged my morale about
posting here. In the future, I may discontinue this blog entirely and
move over to a service like ThoughtStreams, or I may even spin up my
own blog! For the time being, though, books need reviewing, I love
writing, and I haven't made the time to designate any other place as
my primary review blog spot.
