-----
title: Python Cookbook (2013) by David Beazley, Brian K. Jones; O'Reilly Media
tags: review, books, python
date: June 14, 2013
-----

I recently purchased the Python Cookbook. The opinions below are all
mine. This wasn't a gift, or an item gifted to me for review purposes
- I really wanted this book!

Read more below to see why.

## Python 3: The Power is Yours!

This book is an amazing resource for making the most of Python 3. This
book truly makes it apparent what you're missing out on in the land of
Python 2 and shows what can be accomplished more concisely in Python
3.

For example, as early as chapter 1, you're introduced to advanced
unpacking techniques introduced in Python 3:

```python
head, *tail = elems
```

The equivalent in Python 2 is:

```python
head, tail = elems[0], elems[1:]
```

The chapter on generators and iterations techniques alone is worth the
price of this book. It shows you how to "Loop Like a Native", all the
way through using partial function application combined with lambdas
to lazily loop over the contents of a file, ending when there's
nothing left to read. Elegant!

There's more gems here, but I'll leave them for you to discover. The
authors did a wonderful job of putting this book together, and I
highly recommend this to anyone interested in becoming an effective
Pythonista!
