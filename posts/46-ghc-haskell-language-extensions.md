------
title: Presenting -  GHC Language Extensions
date: February 25, 2015
tags: haskell
------

Slides: [link](/files/ghc-lang-exts.pdf)

I gave a presentation a little over a week ago on GHC lkanguage
extensions. This blog post is both a publication of the materials and
a touch of additional commentary. The presentation was a put together
quickly and was meant to explore GHC/Haskell language extensions: what
they are, what they do, how they're used. It goes more for broad
coverage rather than precise details. As a result, particularly when
reviewing extensions like
[GADTs](https://downloads.haskell.org/~ghc/7.10.1-rc2/docs/html/users_guide/data-type-extensions.html#gadt)
or
[Existential Quantification](https://downloads.haskell.org/~ghc/7.10.1-rc2/docs/html/users_guide/data-type-extensions.html#existential-quantification),
the materials are sparse.

Thankfully, the
[GHC 7.10 User Guide](https://downloads.haskell.org/~ghc/7.10.1-rc2/docs/html/users_guide/ghc-language-features.html)
is **far more** thorough than I. :)

As I read through the user guide, it got me thinking about the base
Haskell language, specifically,
[Haskell 2010](https://www.haskell.org/onlinereport/haskell2010/). That's
already a rather rich language. Excluding the standard library, and
focusing strictly on the language, it features things like:

* Pattern matching
* Conditional matching using if/then/else or guards
* Automatic currying
* A module system
    * Qualified imports (`import qualified X.Y as XY`)
    * Restricted imports (`import X.Y (cat, otherCat)`)
    * Exclusionary imports (`import X.Y hiding (notThatCat)`)
* Sections: `(+1) -- same as (\x -> x + 1)`
* Tuples
* Rich type system
    * Pervasive type inference
    * Polymorphic types: `Maybe a`
    * Higher-kinded types: `Either a b -- with kind Either :: * -> * -> *`
    * Type constraints and open interfaces, ala typeclasses
    * Generative types ala `newtype Age = Age Int`
    * Type synonyms ala `type Pred a = (a -> Bool)`
    * Type definitions allowing for sums, products, and recursion
    * Record types: `data Person = Person {name :: String, ...}`
* Sugar
    * List comprehensions
    * Do-notation

Given that as a base, it's interesting to think about how the language
extensions might have come about. There's an extension for (I think)
every Haskell 2010 core language feature.

Without further commentary, here's the
[slides](/files/ghc-lang-exts.pdf)).

I hope they're helpful to you, too. It was enlightening spending some
time reading through the GHC User Guide to put that together.
