-----
date: April 13, 2014
title: On Polyamory and Relationships
tags: polyamory, me
-----

Thanks to my partner, I discovered
[Kimchi Cuddles](http://kimchicuddles.com/tagged/chrono/chrono)
today. I basically devoured the comic in two sittings. It has so much
sage advice about *having relationships*, that I'm here left
processing all of it now. It's not just about what it means to be
poly, though there's a lot of that in there. It's also about what
happens between real people trying to have relationships - and that is
**amazing**. It's cute, adorable, touching, and healthy.

It was a long time ago that I discovered that polyamory was a thing. I
think... about seven years ago, towards the end of a relationship. I
was thoroughly confused at the time, because I was experiencing
jealousy and compersion, all kinds of new-relationship energy (NRE),
and I had none of the tools to cope! Communication was definitely not
one of my strong points then!

Still, despite the hard times, that was a life-changing moment for
me. To realize that it's *okay* to love people, to love everyone, and
that it can be **talked about**, was as mind-blowing as my recent
explorations into mathematics and type theory. Even recently, I'm
still processing all of this, and finding out just how fundamentally
it shifted my view of the world. For example, not believing that there
can be
[only one](http://kimchicuddles.com/tagged/chrono/chrono/page/28)
person to love has also made it easier for me to accept myself. I
don't need to fit any mold to be loved or to love myself, and this has
enabled me to sincerely pursue who I am.

I'm sort-of-kind-of "out". It's not very often I talk about my beliefs
on relationships and my experiences. I'm still recovering from
pigeon-holing myself into a few
[narrow areas](/posts/2014-31-03-on-being-me.html)
for most of my life. This has the side-effect that I feel
uncomfortable speaking about a few things. I'm challenging that more
and more, though. It's exciting and scary.

There's one more thing I'd like to share before I close this post. I
can relate strongly to the idea of
[spreading awareness](http://kimchicuddles.com/tagged/chrono/chrono/page/299)
of poly- and alternative-relationship models. It's part of why I'm
writing this. I was lost and confused for a good part of growing up,
with little guidance as to how to even have a basic healthy
relationship. Role models for healthy relationships were mostly
missing in my life. My hope is that by writing, by speaking, by
sharing, I can help someone else not be as lost as I was.
