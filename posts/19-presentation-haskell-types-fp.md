-----
title: Presenting -  An Introduction to Haskell, Type Systems, and Functional Programming
date: April 22, 2014
tags: haskell, presentation, types, functional programming
-----

Hello all, tiny post today.

I presented at Rackspace:Atlanta, and the goal of the talk was to
express the idea:

> Let's use the wisdom of more than **four decades** worth of
> programming language theory to write better software.

The material is Haskell-centric, is introductory in nature, but should
apply rather well to any other functional programming language with a
statically-checked type system. In fact, I made explicit mention of
this early on, cheering on the benefits of Scala, F#, Ocaml, ML,
Idris, and others.

Here are the slides: [PDF](/files/intro-haskell.pdf)

Corrections and feedback are welcome over at my
[presentations](https://github.com/queertypes/presentations) repository.

Thanks!
