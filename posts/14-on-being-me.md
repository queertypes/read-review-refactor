---
title: On Being Me
tags: me
date: March 31, 2014
---

Hello world,

I'm writing about me today. It's a topic I don't often write about,
because it's scary. That's the main reason why I'm writing about me -
because I don't want it to be scary. I want to be me, wherever I go,
whatever I am doing, and whoever I am with.

First, a story. 

For over 10 years of my life, I've confined myself to two or three
interests. This limitation felt safe. I felt confident in these areas,
and that confidence helped shield me from the fear of being open.

Computer science

Video games

So really, two. I could go on and on about the right programming
languages and methodologies to use, comparing alternatives. I loved
speaking about and learning about new data structures, their various
asymptotics, and where they were appropriate to use. I collected bits
and pieces of information about advanced research in innumerable areas
of computer science, because it helped protect me from feeling like I
wasn't good enough. Maybe this is that impostor syndrome I've read
about. It was a constant battle against fear of inadequacy.

And video games. They were a retreat. A place I could go and level up,
and improve, and play, without the fear of being seen as less. I could
talk about them, too, and compare them, and celebrate the little
victories. Still, I couldn't use video games to play with others. It
was scary to think that I might invite someone into a place of
retreat.

So that was the me that I let myself be for years. I defined myself so
strictly that when I finally started looking at who I was, I wasn't
even really sure. I often said programmer. I sometimes said
gamer. I've used student, as well. I was okay as long as I didn't have
to step outside of those boxes.

Then, over the past few years, I've painfully realized that I wanted
to be more. This is in no small part due to the patience, diligence,
and understanding of my spouse, Jessica Cabrera.

So here I am today, trying to figure out what it means to be me. How
do I do it? By breaking out of the boxes I've made for myself, I
think. By that, I mean, being open and honest wherever I can be. Here,
too, in this blog that I started originally because the idea of being
able to read books in exchange for reviews sounded awesome. Reader
review programs - I'm still pretty fond of them.

All of my blogging efforts in the past have failed, and I think part
of the cause was that I tried to narrow myself too much. I wrote only
from the filter of my boxes, only one of which I felt safe speaking
about in the open.

So here I am, encouraged by the discussions I see being held in the
open. Questions of gender, of shaming, blaming, of connectivity, of
sexuality, positivity, of empowerment, of the woes of meritocracies,
of the individual, of collaboration and education, faltering political
systems, and of living the life that only I can live. I want to
participate in these discussions, because I don't want to stand by and
silently allow for things to continue as they are.

So here I am. This is me, and some of the many things I care
about. Nice to meet you, and I'll be writing again in the future.

Hello, world.
