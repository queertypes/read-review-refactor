----------
date: October 16, 2015
title: Queer Types -  The Business
tags: business, queer, types
----------

I started my own business yesterday: Queer Types! I'm writing about it
here to tell the story of how Queer Types came to be, and what it is.

## Tech & Capitalism: do you wanna have a bad time?

I've worked in tech for almost three years now. The first year was the
best, and things gradually went down hill from there. A few factors
were at play in that decline.

My awareness of myself and the world around me was **really** low
in 2012. Honestly, I didn't know much about who I was, what I believed
in, or where I was going. I just knew I had to get out of college, and
that getting paid was a Good Thing.

At the time, I had no idea I was trans. I didn't even know it was a
thing I could be. I'm suspect I had some white, male-passing privilege
helping me along. By the time April 2014 rolled around, I knew I was
trans - I was out soon after that. Things gradually got worse at work,
as I presented more and more femme, especially in 2014. 2015 was
better, in this regard. Still, for every job after my first, I was the
only woman on the teams I was assigned to. It's really alienating to
be the only trans person, the only woman, the only X - in any team
situation.

Awareness - that spiked pretty quickly after I realized I was trans. I
learned about all the different ways people are marginalized and
oppressed, that there are irrefutable, intersectional systems that
keep people down. [Twitter](https://twitter.com/) played a significant
role in my awareness increasing over time. I saw painful situations
unfolding in real time. I listened carefully to people expressing
their lived experiences. The first few months of analyzing my
privilege were **very** uncomfortable. It still can be uncomfortable
when I run into something I haven't introspected on before. That got
easier. It gets easier.

I couldn't help but see those structures of oppression paralleled in
every job I had. Expectations of long hours, more hours than my health
could handle. Credit being taken by company "leadership" for the
efforts of the workers. Blame being put on the workers when things
didn't work out as planned. Punishment for the workers when they spoke
up about things being shitty, unrealistic, or unreasonable. I didn't
spend much time blaming individual actors, except for a few
particularly toxic examples. The non-toxic participants were caught up
in these sytems, and they need to make a living, too. My goals were
fundamentally misaligned with things like *Delivering Business Value*,
*Minimum Viable Products*, *Profits*. I'm not suited to quietly
working in companies that want to keep to the exploitative status quo.

In a sense, all that time I'd been working (n+1) jobs: the 1 salaried,
the remaining **n**, unpaid labor. Things like: writing articles about
[tech](https://queertypes.com/), taking **extra** time and care
to ensure the things I made in my day job were of high quality and
maintainable, publishing and maintaining open source
[libraries](https://gitlab.com/u/queertypes), crafting
[tutorials](https://gitlab.com/queertypes/type-assisted-speed-runs),
following and reporting on tech trends. Others have written about all
this
[before](http://www.ashedryden.com/blog/the-ethics-of-unpaid-labor-and-the-oss-community),
several
[people](https://modelviewculture.com/pieces/what-your-open-source-culture-really-says-part-one),
at different
[times](https://modelviewculture.com/pieces/you-say-you-want-diversity-but-we-cant-even-get-internships) -
Unpaid Labor.

Taking inspiration from articles like
[this one](https://modelviewculture.com/news/lets-talk-about-pay) on
wages in tech, or
[this one](https://modelviewculture.com/pieces/giveyourmoneytowomen-the-end-game-of-capitalism),
and realizing I couldn't end Capitalism over night or escape it, I
decided to seek my own path. I wanted to find a way to: work on my
terms, with my ethics and capabilities in mind, and make it
sustainable. If anything, that's the origin story for Queer Types, at
least three years in the making.

## Queer Types: Setting Goals and Boundaries

I want to use the skills I have to help make the world a better
place. I also want it to be on my terms.

Some terms I've set for myself:

* I will not work on secret or proprietary software

I've spent enough time rebuilding CRUD app #37. At large, it's also
notable that we have more than 5 SQL databases, more than 4 web
browsers, probably more than 20 text editors, and so on. There's a lot
of repetition, and for what?

I can understand the need for customization and tailoring tools for
specific problems. However, what we have as a consequence of copyright
and profit-driven-development is a rebuilding of the same fundamental
things, over and over again. This is not a technical problem, this is
a socioeconomic problem.

I want to avoid contributing to that senseless duplication.

* I will not work on secret or proprietary research

Similarly, for research. Knowledge should flow as freely as
possible. It's easier than ever now, too.

* I **will** work on software projects available to all

Generally, this means that the source is available and the
[licensing](http://opensource.org/licenses) aligns with the goal of
keeping the projects available to all.

* I **will** make all my written works freely available

As with software. Yes, this includes books I intend to write.

* I expect to be paid

See the origin story in the previous section. That I want to provide
freely available works doesn't mean I'll allow myself to be
exploited. I have to live, too. I do not condone Unpaid Labor,
**especially** in a society that still thinks it's right for people to have
to Earn Their Right to Live.

* Consent first

I'll be working with others. I'll respect their terms, too, and if
they align, we can work together.

## Queer Types: What Will it Do

Phrased differently, what can I do? I can write software, technical
articles, tutorials, and books. I can also teach, make art, and
consult.

So, keeping the terms and boundaries above in mind, I hope to offer
the following services through Queer Types:

* Technical writing
* Technical workshops
* Written tutorials
* Software: implementation, design, testing, benchmarking
* Drawing: cute things, technical things, cute & technical things
* Awareness & advocacy

Notably, for drawings, this would go great with Patreon-acquired
funding as a reward. You like what I do, I write you a small program,
and then send you a hand-made drawing involving that program. We
celebrate, because things are cute, and we both learn things. This
line of thinking was inspired by
[sailorhg](https://twitter.com/sailorhg)'s work on
[Bubble Sort](http://shop.bubblesort.io/) zines. There's no reason
tech can't be cute. ❤

On tech work specifically, I have no intention of covering the full
software market. I know my specialties and preferences. I love typed,
functional programming. I like backend and systems work, and I want to
like frontend work. Technology-wise, this amounts to work involving
some mix of:

* Linux
* Haskell
* PureScript
* Idris
* Rust, maybe

Projects like
[Support JSON errors](https://github.com/purescript/purescript/issues/1523)
for PureScript (or other error reporting enhancements), improvements
to [cabal](https://github.com/haskell/cabal/issues) or
[stack](https://github.com/commercialhaskell/stack/issues), and other
infrastructural/library work around any of the above languages, would
be suitable for me.

## Finance Models: Attempting Ethical Sustainability

So then, what do I do, and how do I maintain my terms and beliefs
while I'm at it? It's still a bit of an open problem, to be honest. My
first two plans, both with short-comings:

* Start a Patreon for my month-to-month efforts
* Start a KickStarter/IndieGogo campaign for book work

Patreon mostly works, especially for requesting donations in exchange
for cute things. The primary shortcoming is in the allocation of my
resources (hours I can work per month) to various "rewards" tiers for
larger scope work.

For example, I can create a reward tier for $1200/month that serves as
a promise to a particular funder that I will work on a mutually-agreed project
for 4 days. If I introduced more levels (more money, more promised
days), the limit system fails me, because it works in terms of rewards
given, rather than the global hours-I-can-work-per-month
resource. Example:

* 10 slots: $600/month for 2d of project work
* 5 slots: $1200/month for 4d of project work

I can end up overcommitted with:

* 4 instances of $600/month: 8d of work
* 4 instances of $1200/month: 20d of work
* Total: 28d of work

Also, things like:

* Negotiating projects with potential funders (accept/reject/clarify/allocate)
* Scheduling/managing projects (in progress/outcome reporting/pending)

would have to happen out-of-band. Might be a worthwhile weekend
project to scrap together a site that interacts with a service like
Stripe for payment processing but also gives me (and funders) the
ability to visibly negotiate and track projects. I dunno. Open problem!

The IndieGogo campaign may be the best short term path. I have plans
to write an accessible book on **Haskell Web Development**. I'd ask
for $20000 as a minimum marker for writing such a book over a period
of 3 months. That'd cover the cost of living for longer than 3 months,
and there'd likely be weekly chapter releases, so funders can watch
the book grow over time. Stretch goals would cover additional topics,
additional mediums (screen casts?), and possibly an additional book
(anything past the $60,000 mark).

Keeping with the previous section on my terms, the book would be
freely available. It'd be akin to
[Real World Haskell](http://book.realworldhaskell.org/) or
[PureScript by Example](https://leanpub.com/purescript) - people can
choose to pay for print copies, and Amazon/LeanPub/etc. can provide
them, but the sources to build PDFs/etc. and run code will always be
available in some source control repo. Also akin to the Bandcamp model
for artists, where some works are offered on a "Name Your Price"
basis: you can download it for free if you like, but if you're able,
donations are welcome.

Another possibility for funding would be to look towards something
like [Ruby Together](https://rubytogether.org/). It seems that the
[Industrial Haskell Group](http://industry.haskell.org/) is the
closest analogue to this in Haskell land. It doesn't sound anywhere
near as appealing or welcoming as the Ruby Together effort. Maybe
there's room for another such organization? I don't know. More open
problems!

## Closing

That's the gist of it. I don't want to work in our current systems, I
want to set my boundaries, and I have a plan.

Next steps: figure out funding. You'll all hear about that part pretty
soon.

Long term goals: end capitalism?
