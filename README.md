Welcome to Read, Review, Refactor!

Herein lies the source and all the posts. If you'd like to build a local version:

1. Install [Haskell stack](http://docs.haskellstack.org/en/stable/README/#how-to-install)
2. stack build
3. stack exec Blog -- watch

Powered by [Hakyll](http://jaspervdj.be/hakyll/).
