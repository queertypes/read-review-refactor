--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}

import Data.Monoid (mconcat, (<>))
import Hakyll

feedConf :: FeedConfiguration
feedConf = FeedConfiguration
    { feedTitle = "Read, Review, Refactor"
    , feedDescription = "Thoughts on technology and life."
    , feedAuthorName = "Allele Dev"
    , feedAuthorEmail = "allele.dev@gmail.com"
    , feedRoot = "https://queertypes.com"
    }

--------------------------------------------------------------------------------
main :: IO ()
main = hakyll $ do
  tags <- buildTags "posts/*" $ fromCapture "tags/*.html"
  let assets = ["images/*", "files/*"]
      templatize = loadAndApplyTemplate

  match (foldr1 (.||.) assets) $ route idRoute >> compile copyFileCompiler
  match "css/*" $ route idRoute >> compile compressCssCompiler
  match "js/*" $ route idRoute >> compile copyFileCompiler

  match (fromList ["about.md", "contact.md"]) $
    route (setExtension "html") >>
    compile (pandocCompiler >>=
             templatize "templates/default.html" defaultContext >>=
             relativizeUrls)

  match "posts/*" $
      route (setExtension "html") >>
      compile (pandocCompiler >>=
               saveSnapshot "content" >>=
               templatize "templates/default.html" defaultContext >>=
               relativizeUrls)

  create ["archive.html"] $ do
      route idRoute
      compile $ do
          posts <- recentFirst =<< loadAll "posts/*"
          let ctx = archiveCtx posts
          makeItem ""
              >>= templatize "templates/archive.html" ctx
              >>= templatize "templates/default.html" defaultContext
              >>= relativizeUrls

  match "index.html" $ do
      route idRoute
      compile $ do
          posts <- fmap (take 10) . recentFirst =<< loadAll "posts/*"
          let ctx = indexCtx posts
          getResourceBody
              >>= applyAsTemplate ctx
              >>= templatize "templates/default.html" defaultContext
              >>= relativizeUrls

  tagsRules tags $ \tag pattern -> do
      let title = "Posts tagged: " <> tag
      route idRoute
      compile $ do
          posts <- loadAll pattern
          let ctx = constField "title" title <>
                    listField "posts" (postCtx) (return posts) <>
                    defaultContext

          makeItem ""
               >>= templatize "templates/posts.html" ctx
               >>= templatize "templates/default.html" defaultContext
               >>= relativizeUrls

  match "templates/*" $ compile templateCompiler

  let feedCtx = postCtx <> bodyField "description"
      ps = loadAllSnapshots "posts/*" "content"
      recent n = fmap (take n) . recentFirst
      createFeed path f = create [path] $ route idRoute >> compile f

  createFeed "atom.xml" (ps >>= recent 10 >>= renderAtom feedConf feedCtx)
  createFeed "rss.xml" (ps >>= recent 10 >>= renderRss feedConf feedCtx)
  createFeed "atom-haskell.xml" (taggedPosts tags "haskell" <$> ps >>=
                                 recent 10 >>= renderAtom feedConf feedCtx)
  createFeed "rss-haskell.xml" (taggedPosts tags "haskell" <$> ps >>=
                                recent 10 >>= renderRss feedConf feedCtx)

--------------------------------------------------------------------------------
taggedPosts :: Tags -> String -> [Item String] -> [Item String]
taggedPosts tags tag posts =
  let tagged = lookup tag $ tagsMap tags
      idIn = elem . itemIdentifier
      filterByTag (Just ts) ps = filter (`idIn` ts) ps
      filterByTag Nothing _ = []
  in
      filterByTag tagged posts

indexCtx :: [Item String] -> Context String
indexCtx posts = mconcat
  [ listField "posts" (postCtx) (return posts)
  , constField "title" "Home"
  , defaultContext
  ]

postCtx :: Context String
postCtx = mconcat
    [ dateField "date" "%B %e, %Y"
    , defaultContext
    ]

archiveCtx :: [Item String] -> Context String
archiveCtx posts = mconcat
  [ listField "posts" (postCtx) (return posts)
  , constField "title" "Archives"
  , defaultContext
  ]
