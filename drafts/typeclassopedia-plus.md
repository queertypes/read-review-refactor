-----
title: A Tour of the Typeclassopedia
date: June 17, 2014
tags: haskell
-----

## Functor

```haskell
class Functor f where
    fmap :: (a -> b) -> f a -> f b

fmap id = id  --^ identity
fmap (g . h) = (fmap g) . (fmap h)  --^ composition
```

## Applicative

```haskell
class Functor f => Applicative f where
    pure :: a -> f a
    (<*>) :: f (a -> b) -> f a -> f b

pure id <*> v = v  --^ identity
pure f <*> pure x = pure (f x)  --^ homomorphism
u <*> pure y = pure ($ y) <*> u  --^ interchange
u <*> (v <*> w) = pure (.) <*> u <*> v <*> w  --^ composition
fmap g x = pure g <*> x  --^ functor relationship
```

# Monad

```haskell
class {-- Applicative m => --} Monad m where
    return :: a -> m a
    (>>=) :: m a -> (a -> m b) -> m b
    (>>) :: m a -> m b -> m b
    m >> n = m >>= \_ -> n

-- formulation using (>>=)
return a >>= k = k a
m >>= return = m
m >>= (\x -> k x >>= h) = (m >>= k) >>= h
fmap f xs = xs >>= return . f = liftM f xs

-- in terms of (>=>)
(>=>) :: Monad m => (a -> m b) -> (b -> m c) -> a -> m c
return >=> g = g  --^ return is identity
g >=> return = g
(g >=> h) >=> k = g >=> (h >=> k)  --^ associativity
```

# Semigroup

```haskell
class Semigroup a where
    (<>) :: a -> a -> a

(x <> y) <> z = x <> (y <> z)  --^ associativity
```

# Monoid

class Monoid a where
    mempty :: a
    mappend :: a -> a -> a  --^ see Semigroup:(<>)

mempty `mappend` x = x  --^ identity
x `mappend` mempty = x
(x `mappend` y) `mappend` z = x `mappend` (y `mappend` z)

# Other Monoids: Alternative

```haskell
class Applicative f => Alternative f where
    empty :: f a
    (<|>) :: f a -> f a -> f a
```

# Other Monoids: MonadPlus

```haskell
class Monad m => MonadPlus m where
    mzero :: m a
    mplus :: m a -> m a -> m a
```

# Other Monoids: ArrowZero

```haskell
class Arrow arr => ArrowZero arr where
    zeroArrow :: b `arr` c
```

# Other Monoids: ArrowPlus

```haskell
class ArrowZero arr => ArrowPlus arr where
    (<+>) :: (b `arr` c) -> (b `arr` c) -> (b `arr` c)
```

# Foldable

```haskell
class Foldable t where
    fold :: Monoid m => t m -> m
    foldMap :: Monoid m => (a -> m) -> t a -> m
    foldr :: (a -> b -> b) -> b -> t a -> b
    foldl :: (a -> b -> a) -> a -> t b -> a
    foldr1 :: (a -> a -> a) -> t a -> a
    foldl1 :: (a -> a -> a) -> t a -> a    
```

* Need to only implement one of ```fold``` or ```foldMap``` to get
  them all.
  
# Traversable

```haskell
class (Functor t, Foldable t) => Traversable t where
    traverse  :: Applicative f => (a -> f b) -> t a -> f (t b)
    sequenceA :: Applicative f => t (f a) -> f (t a)
    mapM      ::       Monad m => (a -> m b) -> t a -> m (t b)
    sequence  ::       Monad m => t (m a) -> m (t a)

traverse Identity = Identity
traverse (Compose . fmap g . f) =
    Compose . fmap (traverse g) . (traverse f)
```


# Category

```haskell
clas Category arr where
   id  :: cat a a
   (.) :: cat b c -> cat a b -> cat a c
```

```haskell
newtype Kleisli m a b = Kleisli { runKlesli :: a -> m b }
instance Monad m => Category (Kleisli m) where
    id = Kleisli return
    Kleisli g . Kleisli h = Kleisli (h >=> g)
```

# Arrow

```haskell
class Category arr => Arrow arr where
    arr :: (b -> c) -> (b `arr` c)
    first :: (b `arr` c) -> ((b, d) `arr` (c, d))
    second :: (b `arr` c) -> ((d, b) `arr` (d, c))
    (***) :: (b `arr` c) -> (b' `arr` c') -> ((b, b') `arr` (c, c'))
    (&&&) :: (b `arr` c) -> (b `arr` c') -> (b `arr` (c, c'))
```
