-----
title: The Most Important Part of Software is People
date: Dec. 18, 2014
tags: people, software
-----

I've given software development a lot of thought over the past
year. This is the same year that I:

* picked up Haskell in anger
* learned about intersectional feminism
* came out as transgender, non-binary

I've also worked in several teams over the span of my software
development career, in all sorts of contexts: in college, for group
assignments; in industry, with local and distributed teams; for fun,
alone and with others.

I've come to the conclusion that the most important part of software
development isn't:

* the choice of programming language
* the choice of text editor
*
