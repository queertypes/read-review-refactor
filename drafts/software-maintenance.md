-----
title: Thoughts on Software Maintenance as a Haskeller
date: November 4, 2014
tags: types, haskell, maintenance
-----

In working on my own projects over the past few weeks, and contrasting
this to my day job, I've come to realize yet again that:

I **miss** effect tracking.

It's what's inspired me to write this post.

Given that a large portion of the work that goes on in the software
industry today (in my professional experience) is software
**maintenance**, effect tracking is a blessing.
