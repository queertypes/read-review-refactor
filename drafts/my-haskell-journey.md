---
title: My Haskell Journey
date: July 15, 2014
tags: haskell. type theory, math, programming
---

Univalent Foundations (slides), Vladimir Voevodsky (29pgs)

Or - why mathematics was in dire need of a new foundation, with my commentary below. The slides are wonderful and succinct. They explain why we need computerized proofs for mathematics and how we're getting there.

---

We've depended on set theory (in particular, ZFC (1922) ) for some time now. It informs both our mathematical research and our programming methodologies. Something is or isn't in a particular set, with associated proofs.

The problem with ZFC is that it makes it difficult to express the hierarchical structure of mathematics.

So then came category theory (1942-1945), which informed many interesting developments. It was about mappings between structures, and abstractly, mappings within those mappings. It was a mathematical weaving. This continued infinitely upwards.

At a point, it was determined that a particular formulation of categorial logic, topos, could serve as a foundation for mathematics in the stead of set theory. These admitted a direct mapping between classical logic. One particularly relevant to programmers is known as the Curry-Howard isomorphism: propositions as types. (Logic <-> Types <-> Categories).

Here's where things get fuzzy for me, and I defer to other texts and the slides above.

Univalent foundations (2013, homotopy type theory) expands on category theory. It considers structures and isomorphisms (iso -> "having the same form"). It's an extremely active area of research that will change the way we do both mathematics and programming.

---

Things like Haskell, ML, Scala, F#, Rust, and Swift are points in this space. They're more advanced than what's common in programming industry, and let us prove things about our programs before we ever run them. These languages and tools are starting to catch on now. They provide a convenient leaping board to reuse many of the ideas from mathematics in our programs and vice-versa.

While they each provide great libraries for building the systems we need today, they are the result of the state of the art in the late 20th century. Design patterns are informed by category theory, and the extent of abstraction we can model soundly is limited by Hindley-Milner type systems. It already represents a great deal of power, though. Recursive types are easy to express in these languages and that's an almost unthinkable leap over my C++/Python roots of the past!

In any case, I can only begin to imagine what we'd be able to prove and model given access to languages based on homotopy type theory. I'm thrilled and enthralled by what awaits.
