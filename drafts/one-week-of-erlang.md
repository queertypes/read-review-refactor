-----
title: One Week of Erlang
date: November 14, 2014
tags: erlang, types
-----

## Why Erlang?

## A Functional Language

## Map, Filter, Fold, ZipWith, Compose, Curry, Uncurry

## A Nifty Type System: Specs and Dialyzer

## Error Philosophy

## Separating Effectful from Effect-Free Computation

## Lots of Great Reading

## Package Management: Applications and Rebar

## What's Next?
