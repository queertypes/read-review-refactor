-----
title: Math for the Working Programmer
date: July 18, 2014
tags: math, programming, algebra, category theory, types
-----

## Algebra

* Programming in the small
* Reasoning by properties:
  - Associative
  - Distributive
  - Commutative
  - Identity
  - Zero
  - Transitivity
  - Symmetry
  - Antisymmetry
  - Equality
  - Relations
* Allows for the reuse of abstractions
  - Monoids
  - Semigroups
* Concise, complete, closed, consistent, composable
* Examples of algebra in action:
  - [CRDTs](http://hal.inria.fr/docs/00/55/55/88/PDF/techreport.pdf) for distributed eventual consistency
  - [Monoids](http://www.cis.upenn.edu/~byorgey/pub/monoid-pearl.pdf) for layering images
  - [Relational algebra](http://www.seas.upenn.edu/~zives/03f/cis550/codd.pdf) for fundamentals of relational DBs

## Category Theory

* Two notions: composition and transformations
* Allows for larger reasoning
  - How do the pieces of a system fit together?
  - How do you go from one form of data to another?
  - How do you maintain structure throughout?
* More resuable abstractions:
  - Monads
  - Arrows
  - String diagrams
  - Functors

## Type Theory

* Inductive representations of all algorithms and data
* Proofs by construction via MLTT/intuitionistic logic
