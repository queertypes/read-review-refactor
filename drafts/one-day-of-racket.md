-----
title: One Day of Racket
date: November 15, 2014
tags: racket, types
-----

## Why Racket?

## A Modern Scheme

## Functional Fundamentals

## A Nifty Type System: Typed Racket + emacs

## Coursera and Racket

## Package Management: raco

## What's Next?
