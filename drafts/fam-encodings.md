-----
title: (Draft/WIP) Functor/Applicative/Monad Across Languages
date: October 2, 2015
tags: haskell, fsharp, ocaml, rust, scala, purescript, elm, agda, idris, erlang, clojure
-----

This is a promise. Placeholder for now, based on these
[tweets](https://twitter.com/queertypes/status/623928842780934145).

### Abstractions

* Maybe
* Either
* State

### Higher-kinded Languages

* Haskell
* Scala
* Purescript

### Single-kinded Languages

* OCaml
* F#
* Elm
* Rust

### Dynamically/Gradually-typed Languages

* Erlang
* Clojure
* Racket

### Dependently-typed Languages

* Idris
* Agda
