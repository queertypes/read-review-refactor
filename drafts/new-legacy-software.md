-----
title: New Legacy Software
date: April 10, 2014
tags: testing, haskell
-----

Write about how testing isn't enough, that we need to start proving
our software correct.
