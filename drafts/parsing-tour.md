-----
date: August 20, 2014
title: A Tour of Parsing (1)
tags: haskell, parsing
-----

## Functor

```haskell
class Functor f where
  fmap :: (a -> b) -> f a -> f b
```

## Monad

```haskell
class Monad m where
  (>>=) :: forall a b. m a -> (a -> m b) -> m b
  (>>) :: forall a b. m a -> m b -> m b
  return :: a -> m a
```

## Monad Functions

```haskell
mapM :: Monad m => (a -> m b) -> [a] -> m [b]
mapM_ :: Monad m => (a -> m b) -> [a] -> m ()
forM :: Monad m => [a] -> (a -> m b) -> m [b] 
forM_ :: Monad m => [a] -> (a -> m b) -> m ()
sequence :: Monad m => [m a] -> m [a]
sequence_ :: Monad m => [m a] -> m ()
(=<<) :: Monad m => (a -> m b) -> m a -> m b
(>=>) :: Monad m => (a -> m b) -> (b -> m c) -> a -> m c
(<=<) :: Monad m => (b -> m c) -> (a -> m b) -> a -> m c
forever :: Monad m => m a -> m b 
void :: Functor f => f a -> f ()
```

## MonadPlus

```haskell
class Monad m => MonadPlus m where
  mzero :: m a
  mplus :: m a -> m a -> m a
```

## Generalized List Operations

```haskell
join :: Monad m => m (m a) -> m a
msum :: MonadPlus m => [m a] -> m a -- concat
mfilter :: MonadPlus m => (a -> Bool) -> m a -> m a
filterM :: Monad m => (a -> m Bool) -> [a] -> m [a]
zipWithM :: Monad m => (a -> b -> m c) -> [a] -> [b] -> m [c]
zipWithM_ :: Monad m => (a -> b -> m c) -> [a] -> [b] -> m ()
foldM :: Monad m => (a -> b -> m a) -> a -> [b] -> m a
foldM_ :: Monad m => (a -> b -> m a) -> a -> [b] -> m ()
replicateM :: Monad m => Int -> m a -> m [a]
replicateM_ :: Monad m => Int -> m a -> m ()
```

## Conditional Execution

```haskell
guard :: MonadPlus m => Bool -> m ()
when :: Monad m => Bool -> m () -> m ()
unless :: Monad m => Bool -> m () -> m ()
```

## Applicative

```haskell
class Applicative f where
  pure :: a -> f a
  (<*>) :: f (a -> b) -> f a -> f b
  (*>) :: f a -> f b -> f b
  (<*) :: f a -> f b -> f a
```

## Applicative Laws

```haskell
-- identity
pure id <*> v = v

-- composition
pure (.) <*> u <*> v <*> w = u <*> (v <*> w)

-- homomorphism
pure f <*> pure x = pure (f x)

-- interchange
u <*> pure y = pure ($ y) <*> u

-- in relation to Functor
fmap f x = pure f <*> x

-- in relation to Monad
pure = return
(<*>) = ap
```

## Alternatives

```haskell
class Applicative f => Alternative f where
  empty :: f a 
  (<|>) :: f a -> f a -> f a
  some :: f a -> f [a] 
  many :: f a -> f [a]
```

## Utilities

```haskell
(<$>) :: Functor f => (a -> b) -> f a -> f b
(<$) :: Functor f => a -> f b -> f a
(<$) = fmap . const
(<**>) :: Applicative f => f a -> f (a -> b) -> f b
liftA :: Applicative f => (a -> b) -> f a -> f b
optional :: Alternative f => f a -> f (Maybe a)
```

## Bits

```haskell
class Eq a => Bits a where
  (.&.) :: a -> a -> a
  (.|.) :: a -> a -> a
  xor :: a -> a -> a
  complement :: a -> a 
  shift :: a -> Int -> a
  rotate :: a -> Int -> a
  zeroBits :: a 
  bit :: Int -> a 
  setBit :: a -> Int -> a 
  clearBit :: a -> Int -> a
  complementBit :: a -> Int -> a 
  testBit :: a -> Int -> Bool
  bitSizeMaybe :: a -> Maybe Int
  isSigned :: a -> Bool
  shiftL :: a -> Int -> a
  unsafeShiftL :: a -> Int -> a 
  shiftR :: a -> Int -> a
  unsafeShiftR :: a -> Int -> a 
  rotateL :: a -> Int -> a
  rotateR :: a -> Int -> a
  popCount :: a -> Int
```

## Parsers: Top Level

```haskell
type Parser = Parser ByteString
type Result = IResult ByteString
data IResult i r =
    Fail i [String] String
  | Partial (i -> IResult i r)
  | Done i r
```

## Parsers: Running

```haskell
parse :: Parser a -> ByteString -> Result a
feed :: Monoid i => IResult i r -> i -> IResult i r
parseOnly :: Parser a -> ByteString -> Either String a
parseWith :: Monad m =>
     m ByteString
  -> Parser a
  -> ByteString
  -> m (Result a)
parseTest :: Show a => Parser a -> ByteString -> IO ()
```

## Parsers: Parsing

```haskell
word8 :: Word8 -> Parser Word8
anyWord8 :: Parser Word8
notWord8 :: Word8 -> Parser Word8
satisfy :: (Word8 -> Bool) -> Parser Word8
satisfyWith :: (Word8 -> a) -> (a -> Bool) -> Parser a
skip :: (Word8 -> Bool) -> Parser ()

peekWord8 :: Parser (Maybe Word8)
peekWord8' :: Parser Word8

inClass :: String -> Word8 -> Bool
halfAlphabet = inClass "a-nA-N"
notInClass :: String -> Word8 -> Bool

string :: ByteString -> Parser ByteString
skipWhile :: (Word8 -> Bool) -> Parser ()
take :: Int -> Parser ByteString
scan :: s -> (s -> Word8 -> Maybe s) -> Parser ByteString
takeWhile :: (Word8 -> Bool) -> Parser ByteString
takeWhile1 :: (Word8 -> Bool) -> Parser ByteString
takeTill :: (Word8 -> Bool) -> Parser ByteString

takeByteString :: Parser ByteString
takeLazyByteString :: Parser ByteString
```

## Parsing: Combinators

```haskell
try :: Parser i a -> Parser i a

(<?>) :: Parser i a -> String -> Parser i a

choice :: Alternative f => [f a] -> f a
count :: Monad m => Int -> m a -> m [a]
option :: Alternative f => a -> f a -> f a

many' :: MonadPlus m => m a -> m [a]
many1 :: Alternative f => f a -> f [a]
many1' :: MonadPlus m => m a -> m [a]
manyTill :: Alternative f => f a -> f b -> f [a]
manyTill' :: MonadPlus m => m a -> m b -> m [a]

sepBy :: Alternative f => f a -> f s -> f [a]
sepBy' :: MonadPlus m => m a -> m s -> m [a]
sepBy1 :: Alternative f => f a -> f s -> f [a]
sepBy1' :: MonadPlus m => m a -> m s -> m [a]

skipMany :: Alternative f => f a -> f ()
skipMany1 :: Alternative f => f a -> f ()

eitherP :: Alternative f => f a -> f b -> f (Either a b)

match :: Parser a -> Parser (ByteString, a)
```
