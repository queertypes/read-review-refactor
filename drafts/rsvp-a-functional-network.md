-----
title: RSVP, a Functional Network Protocol
date: November 5, 2014
tags: distributed, fp, haskell
-----

I was reading the HTTP/1.1 spec earlier today in preparation for some
upcoming interviews. While doing so, I encountered a sentence that
stopped me in my tracks:

> HTTP was originally designed to be usable as an interface to
> distributed object systems.

Distributed. Object. Systems. My own association with all things
object-oriented reminded me of how inherently stateful and sequential
this model is. Many of our networked systems and interfaces are
modeled this way, which is unfortunate. This is the nature and intent
of HTTP. Even with HTTP/2.0 around the corner, we're still only
addressing point-to-point topologies and encouraging that **as the
common case**.

So this got me thinking, in part because I've been Haskell-ing for a
little over a year - what would a distributed functional system look
like? What if we took common patterns from FP (maps, folds, filters,
zips, unfolds, ...) and reified them as the **core** of a distributed
communication and data processing protocol?

## Provenance

## Generalizing Over Topologies

## Equating Common Patterns

## Modeling Resources with RSVP

##
