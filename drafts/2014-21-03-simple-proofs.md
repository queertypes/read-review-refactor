---
title: Leveraging Types for Simple Proofs
tags: haskell, proofs, types
date: March 21, 2014
---

You can define a type:

```haskell
import Codec.Compression.GZip as GZ

newtype Compressed = Compressed {content :: ByteString}

gzcompress :: ByteString -> Compressed
gzcompress data = Compressed $ GZ.compress data
```

And now the compiler will enforce that data that has been compressed
cannot be compressed again.

```haskell
let x = gzcompress ""
GZ.compress x

<interactive>:11:13:
Couldn't match expected type `LB.ByteString'
            with actual type `Compressed'
In the first argument of `compress', namely `x'
In the expression: compress x
In an equation for `it': it = compress

gzcompress x

<interactive>:12:12:
Couldn't match expected type `LB.ByteString'
            with actual type `Compressed'
In the first argument of `gzcompress', namely `x'
In the expression: gzcompress x
In an equation for `it': it = gzcompress x
```
