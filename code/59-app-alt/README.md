# Applicatives and Alternatives

This is the code for the blog
post,
[Applicatives and Alternatives](https://queertypes.com/posts/59-applicatives-alternatives.html).

The example code is fully interactive. You'll
need [stack](https://docs.haskellstack.org/en/stable/README/) to play
with it.

* To install the GHC compiler, if it's missing: `stack setup`
* To build the code: `stack build`
* To play with it in the REPL: `stack ghci`
* To run tests: `stack exec doctests`
