{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_GHC -fno-warn-unused-top-binds #-}
module FiftyNine (
  Option(..),
  Xor(..),
  Validation(..),

  NumberError(..),
  validateNumber,
  validateNumberV
) where

-------------------------------------------------------------------------------
-- Imports
-------------------------------------------------------------------------------
import Prelude
import Control.Applicative
import Data.Monoid

-- ============================================================================
-- Option/Maybe Type
-- ============================================================================
data Option a
  = Some a
  | None
  deriving Show

instance Functor Option where
  fmap f (Some a) = Some (f a)
  fmap _ None     = None

instance Applicative Option where
  pure = Some
  Some f <*> Some a = Some (f a)
  None   <*> Some _ = None
  Some _ <*> None   = None
  None   <*> None   = None

instance Alternative Option where
  empty = None
  Some a <|> Some _ = Some a
  Some a <|> None   = Some a
  None   <|> Some a = Some a
  None   <|> None   = None


-------------------------------------------------------------------------------
-- Option Examples
-------------------------------------------------------------------------------
add3 :: Num a => a -> a -> a -> a
add3 a b c = a + b + c

add3Opt :: Num a => Option a -> Option a -> Option a -> Option a
add3Opt a b c =
  case a of
    None -> None
    (Some a') -> case b of
      None -> None
      (Some b') -> case c of
        None -> None
        (Some c') -> Some (add3 a' b' c')

isNone :: Option a -> Bool
isNone None = True
isNone _ = False

-- dangerous
getOption :: Option a -> a
getOption (Some a) = a
getOption None = error "oh no crash"

f2 :: Num a => Option a -> Option a -> Option a -> Option a
f2 a b c =
  if isNone a
  then None
  else if isNone b
       then None
       else if isNone c
            then None
            else Some (add3 (getOption a) (getOption b) (getOption c))

f3 :: (Applicative f, Num b) => f b -> f b -> f b -> f b
f3 a b c = add3 <$> a <*> b <*> c

-------------------------------------------------------------------------------
-- Option Doctests
-------------------------------------------------------------------------------

-- | Option examples
-- >>> (+) <$> Some 1 <*> Some 2
-- Some 3
-- >>> (+) `fmap` Some 1 <*> Some 2
-- Some 3
-- >>> (+) <$> Some 1 <*> None
-- None
-- >>> add3 a b c = a + b + c
-- >>> :t add3
-- add3 :: Num a => a -> a -> a -> a
-- >>> add3 <$> Some 1 <*> Some 2 <*> Some 3
-- Some 6
-- >>> add3 <$> Some 1 <*> None <*> Some 3
-- None
-- >>> None <|> Some 1
-- Some 1
-- >>> None <|> None
-- None
-- >>> None <|> None <|> Some 3
-- Some 3

-- ============================================================================
-- Xor/Either Types
-- ============================================================================
data Xor a b
  = XLeft a
  | XRight b
  deriving Show

instance Functor (Xor a) where
  fmap f (XRight a) = XRight (f a)
  fmap _ (XLeft a)  = XLeft a

instance Applicative (Xor a) where
  pure = XRight
  XRight f <*> XRight a = XRight (f a)
  XRight _ <*> XLeft a  = XLeft a
  XLeft a  <*> XRight _ = XLeft a
  XLeft a  <*> XLeft _  = XLeft a -- choose the first error to short-circuit

-------------------------------------------------------------------------------
-- Xor/Either Examples
-------------------------------------------------------------------------------
data NumberError
  = NumberTooBig
  | NumberTooSmall
  | NumberNotAllowed
  deriving Show

validateNumber :: Int -> Xor NumberError Int
validateNumber x
  | x > 500 = XLeft NumberTooBig
  | x < 30 = XLeft NumberTooSmall
  | x `elem` [42, 69, 420] = XLeft NumberNotAllowed
  | otherwise = XRight x

-------------------------------------------------------------------------------
-- Xor/Either Doctests
-------------------------------------------------------------------------------

-- | Xor worked examples
--
-- >>> validateNumber 32
-- XRight 32
-- >>> validateNumber 69
-- XLeft NumberNotAllowed
-- >>> validateNumber 501
-- XLeft NumberTooBig
-- >>> validateNumber 29
-- XLeft NumberTooSmall
-- >>> (+) <$> validateNumber 31 <*> validateNumber 33
-- XRight 64
-- >>> (+) <$> validateNumber 31 <*> validateNumber 42
-- XLeft NumberNotAllowed

-- ============================================================================
-- Validation Types
-- ============================================================================
data Validation a b
  = Failure a
  | Success b
  deriving Show

instance Functor (Validation a) where
  fmap f (Success a) = Success (f a)
  fmap _ (Failure a) = Failure a

-- accumulating errors
instance Monoid a => Applicative (Validation a) where
  pure = Success
  Success f <*> Success a = Success (f a)
  Failure a <*> Success _ = Failure a
  Success _ <*> Failure a = Failure a
  Failure a <*> Failure b = Failure (a <> b)

instance Monoid a => Alternative (Validation a) where
  empty = Failure mempty
  Success a <|> Success _ = Success a
  Success a <|> Failure _ = Success a
  Failure _ <|> Success a = Success a
  Failure _ <|> Failure a = Failure a

-------------------------------------------------------------------------------
-- Validation Examples
-------------------------------------------------------------------------------
numberTooSmall :: Validation [NumberError] a
numberTooSmall = Failure [NumberTooSmall]

numberTooBig :: Validation [NumberError] a
numberTooBig = Failure [NumberTooBig]

numberNotAllowed :: Validation [NumberError] a
numberNotAllowed = Failure [NumberNotAllowed]

validateNumberV :: Int -> Validation [NumberError] Int
validateNumberV x
  | x > 500 = numberTooBig
  | x < 30 = numberTooSmall
  | x `elem` [42, 69, 420] = numberNotAllowed
  | otherwise = Success x


-- | Validation doctests
--
-- >>> validateNumberV 32
-- Success 32
-- >>> validateNumberV 69
-- Failure [NumberNotAllowed]
-- >>> add3V a b c = a + b + c
-- >>> add3V <$> validateNumberV 32 <*> validateNumberV 33 <*> validateNumberV 34
-- Success 99
-- >>> add3V <$> validateNumberV 42 <*> validateNumberV 69 <*> validateNumberV 420
-- Failure [NumberNotAllowed,NumberNotAllowed,NumberNotAllowed]
-- >>> add3V <$> validateNumberV 42 <*> validateNumberV 10 <*> validateNumberV 50
-- Failure [NumberNotAllowed,NumberTooSmall]
-- >>> :{
--     (+) <$> validateNumberV 42 <*> validateNumberV 10
-- <|> (+) <$> validateNumberV 41 <*> validateNumberV 31
-- :}
-- Success 72
