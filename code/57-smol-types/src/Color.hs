module Color where

data Color = Red | Green | Blue | Cyan

colorToInt4 :: Color -> Int
colorToInt4 c =
  case c of
    Red -> 0
    Green -> 1
    Blue -> 2

f = colorToInt4 Cyan
