# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/sda";
  };

  # non-free: allow
  nixpkgs.config.allowUnfree = true;
  hardware.pulseaudio.enable = true;

  # networking.hostName = "nixos"; # Define your hostname.
  networking = {
    hostId = "9cfa138f";
    networkmanager.enable = true;
  };

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "lat9w-16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  environment.systemPackages = with pkgs;
    [ wget
    ];

  services.openssh.enable = true;
  services.ntp.enable = true;
  services.printing.enable = true;

  services.xserver = {
    enable = true;
    displayManager.lightdm.enable = true;
    desktopManager.gnome3.enable = true;
    synaptics = {
      enable = true;
      twoFingerScroll = true;
    };
  };

  users.extraUsers = {
    allele = {
      createHome = true;
      description = "Allele Dev";
      extraGroups = [ "wheel" "networkmanager" ];
      group = "users";
      home = "/home/allele";
      shell = "/run/current-system/sw/bin/bash";
      uid = 10000;
    };
  };

  time.timeZone = "America/Chicago";

  boot.extraModprobeConfig = ''
      options snd slots=snd-hda-intel
      options snd_hda_intel enable=0,1
  '';

  boot.blacklistedKernelModules = [ "snd_pcsp" ];
}
