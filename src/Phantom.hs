{-# LANGUAGE EmptyDataDecls, OverloadedStrings, GeneralizedNewtypeDeriving #-}

-- enforce abstraction by limiting exported functions
module Data.Fun.With.Phantom (
  bytes, unbytes,
  encode, decode,
  compress, decompress
) where

import Prelude hiding (length)
import Data.ByteString
import Test.QuickCheck
import qualified Data.ByteString.Lazy as BL
import qualified Codec.Compression.GZip as GZ

-- Orphan instances: \o/
instance Arbitrary ByteString where
  arbitrary = fmap pack arbitrary

data Plain
data Encoded
data Decompressed
data Compressed
-- polymorphic phantom types `form` and `compressed`
newtype Bytes form compressed = Bytes ByteString deriving (Arbitrary, Eq, Show)

bytes :: ByteString -> Bytes Plain Decompressed
bytes = Bytes

-- I got lazy for encode/decode. there're at least one library on
-- hackage that presents a ByteString-oriented interface. Since this
-- project was more about the type-level details, I opted to just
-- write what "works" for now
encode :: Bytes Plain Decompressed -> Bytes Encoded Decompressed
encode (Bytes bs) = Bytes bs

decode :: Bytes Encoded Decompressed -> Bytes Plain Decompressed
decode (Bytes bs) = Bytes bs

compress :: Bytes a Decompressed -> Bytes a Compressed
compress (Bytes bs) = Bytes . BL.toStrict . GZ.compress . BL.fromStrict $ bs

decompress :: Bytes a Compressed -> Bytes a Decompressed
decompress (Bytes bs) = Bytes . BL.toStrict . GZ.decompress . BL.fromStrict $ bs

unbytes :: Bytes a b -> ByteString
unbytes (Bytes bs) = bs

-- type signatures are great, and I also decided to leave them off
-- here
prop_bytesUnbytesIdemp bs = (bytes . unbytes $ bs) == bs
prop_unbytesBytesIdemp bs = (unbytes . bytes $ bs) == bs
prop_encodeDecodeIdemp bs = (encode . decode $ bs) == bs
prop_decodeEncodeIdemp bs = (decode . encode $ bs) == bs
prop_decompressCompressIdemp bs = (decompress . compress $ bs) == bs
